﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace CSharp_Explorer
{
    public partial class CSharp_Text_Editor: Form
    {
        #region Variables Declaration
        public class FindClickEventArgs: EventArgs
        {
            public bool ClickNeeded { get; set; }

            public FindClickEventArgs()
            {
                ClickNeeded = false;
            }

            public FindClickEventArgs(bool isClickNeeded)
            {
                ClickNeeded = isClickNeeded;
            }
        }

        private static int tabsCount = 0;
        private Encoding defaultEncoding = Encoding.UTF8;
        private Dictionary<string, Tuple<int, int>> codePages = new Dictionary<string, Tuple<int, int>> {
            { "Unicode (UTF-7)", new Tuple<int, int>(65000, 0) },
            { "Unicode (UTF-8)", new Tuple<int, int>(65001, 1) }, 
            { "Unicode (UTF-16)", new Tuple<int, int>(1200, 2) },
            { "Unicode (UTF-32)", new Tuple<int, int>(12000, 3) }, 
            { "Unicode (UTF-32 Big Endian)", new Tuple<int, int>(12001, 4) },
            { "Central European (Windows-1250)", new Tuple<int, int>(1250, 5) },
            { "Western European (Windows-28591)", new Tuple<int, int>(28591, 6) },
            { "Western European (Windows-1252)", new Tuple<int, int>(1252, 7) },
            { "Greek (Windows-1253)", new Tuple<int, int>(1253, 8) },
            { "Turkish (Windows-1254)", new Tuple<int, int>(1254, 9) },
            { "Hebrew (Windows-1255)", new Tuple<int, int>(1255, 10) },
            { "Arabic (Windows-1256)", new Tuple<int, int>(1256, 11) },
            { "Baltic (Windows-1257)", new Tuple<int, int>(1257, 12) },
            { "Vietnamese (Windows-1258)", new Tuple<int, int>(1258, 13) },
            { "Chinese Simplified (GB18030)", new Tuple<int, int>(54936, 14) },
            { "EUC Japanese", new Tuple<int, int>(51932, 15) },
            { "Cyrillic (Windows-1251)", new Tuple<int, int>(1251, 16) },
            { "Cyrillic (KOI8-R)", new Tuple<int, int>(20866, 17) },
            { "Cyrillic (KOI8-U)", new Tuple<int, int>(21866, 18) },
            { "US-ASCII", new Tuple<int, int>(20127, 19) }};

        public event EventHandler<FindClickEventArgs> OnFindClick = delegate { };
        private FormSearch search = new FormSearch();
        public static CSharp_Text_Editor GetEditor
        {
            get
            {
                if (Application.OpenForms.Count == 1)
                    return new CSharp_Text_Editor();
                else
                    return Application.OpenForms["CSharp_Text_Editor"] as CSharp_Text_Editor;
            }
        }
        #endregion

        private CSharp_Text_Editor()
        {
            InitializeComponent();

            foreach (var codepage in codePages)
            {
                cboOpenEncoding.Items.Add(codepage.Key);
                cboSaveEncoding.Items.Add(codepage.Key);
            }
            cboSaveEncoding.SelectedIndex = 1;

            tabsCount = 0;
            
            Show();
        }

        #region Methods
        public void AddTab(string path = null)
        {
            try
            {
                this.Activate();
                string tabName, tabText;
                if (path != null)
                {
                    tabName = path;
                    tabText = Path.GetFileName(path);
                }
                else
                {
                    tabName = "Новый документ " + (++tabsCount).ToString();
                    tabText = tabName;
                }

                TabPage newTab = new TabPage
                {
                    Text = tabText,
                    Name = tabName
                };

                Bitmap cross = global::CSharp_Explorer.Properties.Resources.cross_small;
                ToolStrip ts = new ToolStrip { Dock = DockStyle.Top, RightToLeft = RightToLeft.Yes, GripStyle = ToolStripGripStyle.Hidden, AutoSize = false, Height = cross.Height + 2 };
                ToolStripButton cancel = new ToolStripButton("", cross, ToolStripButtonCancel_Click);
                cancel.AutoSize = false;
                cancel.Size = cross.Size;
                ts.Items.Add(cancel);
                newTab.Controls.Add(ts);

                ToolStripMenuItem newItem = new ToolStripMenuItem(newTab.Name, null, ToolStripMenuItemTabName_Click);
                newItem.CheckOnClick = true;
                newItem.Checked = true;
                newItem.Name = newTab.Name;
                окнаToolStripMenuItem.DropDownItems.Add(newItem);

                tabs.TabPages.Add(newTab);

                OpenFile(newTab, path);

                tabs.SelectedTab = newTab;
                tabs_SelectedIndexChanged(newTab, EventArgs.Empty);
            }
            catch (Exception ex) { MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void OpenFile(TabPage tab, string path, string encoding = "Unicode (UTF-8)")
        {
            try
            {
                tab.Controls.Remove(GetCurrentTextField);

                Bitmap cross = global::CSharp_Explorer.Properties.Resources.cross_small;
                RichTextBox textField = new RichTextBox
                {
                    Location = new Point(0, cross.Height + 3),
                    Height = tab.Height - cross.Height - 4,
                    Width = tab.Width - 2,
                    Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left,
                    WordWrap = переносПоСловамToolStripMenuItem.Checked,
                    AcceptsTab = true,
                    Multiline = true,
                    HideSelection = false,
                    ContextMenuStrip = cmnuEdit,
                    Tag = new object[] { encoding, codePages[encoding].Item2 },
                };

                textField.SelectionChanged += (object sendler, EventArgs e) =>
                    {
                        int line = textField.GetLineFromCharIndex(textField.SelectionStart);
                        int column = textField.SelectionStart - textField.GetFirstCharIndexFromLine(line);
                        positionLabel.Text = String.Format("Строка {0} Cтолбец {1}", line + 1, column + 1);
                        statusLabel.Text = textField.Modified ? "Изменен" : "Сохранен";
                    };

                textField.GotFocus += (object sendler, EventArgs e) =>
                {
                    int line = textField.GetLineFromCharIndex(textField.SelectionStart);
                    int column = textField.SelectionStart - textField.GetFirstCharIndexFromLine(line);
                    positionLabel.Text = String.Format("Строка {0} Cтолбец {1}", line + 1, column + 1);
                    statusLabel.Text = textField.Modified ? "Изменен" : "Сохранен";
                };

                textField.ModifiedChanged += (object sendler, EventArgs e) =>
                    {
                        statusLabel.Text = textField.Modified ? "Изменен" : "Сохранен";
                    };

                textField.MouseDown += (object sender, MouseEventArgs e) =>
                {
                    if (e.Button != MouseButtons.Right)
                        return;

                    int currentCursorPositionInText = textField.GetCharIndexFromPosition(e.Location);
                    if ((currentCursorPositionInText <= textField.SelectionStart) || (currentCursorPositionInText >= textField.SelectionStart + textField.SelectionLength))
                    {
                        textField.SelectionLength = 0;
                        textField.SelectionStart = currentCursorPositionInText;
                    }
                    mnuMain.Show();
                };

                if (path != null)
                    try { textField.Text = File.ReadAllText(path, Encoding.GetEncoding(codePages[encoding].Item1)); }
                    catch (Exception ex) { MessageBox.Show(this, ex.Message, "Ошибка чтения файла", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                textField.Modified = false;

                tab.Controls.Add(textField);
            }
            catch (Exception ex) { MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private RichTextBox GetCurrentTextField
        {
            get
            {
                RichTextBox textField = null;
                foreach (Control ctrl in tabs.SelectedTab.Controls)
                    if (ctrl is RichTextBox)
                        textField = ctrl as RichTextBox;

                return textField;
            }
        }

        private void CSharp_Text_Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
                return;

            закрытьВсеToolStripMenuItem.PerformClick();
            if (tabs.TabCount > 0)
                e.Cancel = true;
        }
        #endregion

        #region Tab Control Events Handlers
        void ToolStripButtonCancel_Click(object sender, EventArgs e)
        {
            RichTextBox textField = GetCurrentTextField;

            if (textField.Modified)
            {
                DialogResult saveNeeded = MessageBox.Show(this, String.Format("Сохранить изменения в файле\n{0}?", tabs.SelectedTab.Name), "Сохранение изменений", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (saveNeeded == DialogResult.Yes)
                {
                    сохранитьToolStripMenuItem.PerformClick();
                    if (textField.Modified)
                        return;
                }
                else if (saveNeeded == DialogResult.Cancel)
                    return;
            }

            TabPage current = (TabPage)(((ToolStripButton)sender).Owner.Parent);
            tabs.TabPages.Remove(current);
            окнаToolStripMenuItem.DropDownItems.RemoveByKey(current.Name);
        }

        void ToolStripMenuItemTabName_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in окнаToolStripMenuItem.DropDownItems)
                item.Checked = false;
            ((ToolStripMenuItem)sender).Checked = true;

            tabs.SelectedTab = tabs.TabPages[((ToolStripMenuItem)sender).Text];
        }

        private void tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabs.TabCount == 0)
            {
                statusLabel.Text = positionLabel.Text = "";

                сохранитьToolStripMenuItem.Enabled = сохранитьКакToolStripMenuItem.Enabled = редактированиеToolStripMenuItem.Enabled =
                    отменитьToolStripMenuItem.Enabled = вернутьToolStripMenuItem.Enabled = вырезатьToolStripMenuItem.Enabled = копироватьToolStripMenuItem.Enabled =
                    вставитьToolStripMenuItem.Enabled = удалитьToolStripMenuItem.Enabled = найтиToolStripMenuItem.Enabled = найтиДалееToolStripMenuItem.Enabled =
                    заменитьToolStripMenuItem.Enabled = выбратьВсёToolStripMenuItem.Enabled = времяИДатаToolStripMenuItem.Enabled = кодировкаToolStripMenuItem.Enabled =
                    форматToolStripMenuItem.Enabled = переносПоСловамToolStripMenuItem.Enabled = шрифтToolStripMenuItem.Enabled = false;
                return;
            }

            сохранитьToolStripMenuItem.Enabled = сохранитьКакToolStripMenuItem.Enabled = редактированиеToolStripMenuItem.Enabled =
                отменитьToolStripMenuItem.Enabled = вернутьToolStripMenuItem.Enabled = вырезатьToolStripMenuItem.Enabled = копироватьToolStripMenuItem.Enabled =
                вставитьToolStripMenuItem.Enabled = удалитьToolStripMenuItem.Enabled = найтиToolStripMenuItem.Enabled = найтиДалееToolStripMenuItem.Enabled =
                заменитьToolStripMenuItem.Enabled = выбратьВсёToolStripMenuItem.Enabled = времяИДатаToolStripMenuItem.Enabled = кодировкаToolStripMenuItem.Enabled =
                форматToolStripMenuItem.Enabled = переносПоСловамToolStripMenuItem.Enabled = шрифтToolStripMenuItem.Enabled = true;

            foreach (ToolStripMenuItem item in окнаToolStripMenuItem.DropDownItems)
                item.Checked = false;
            ((ToolStripMenuItem)окнаToolStripMenuItem.DropDownItems[tabs.SelectedTab.Name]).Checked = true;

            RichTextBox textField = GetCurrentTextField;
            переносПоСловамToolStripMenuItem.Checked = textField.WordWrap;
            cboOpenEncoding.Text = (string)(((object[])textField.Tag)[0]);
            textField.Focus();
        }
        #endregion

        #region Menu Events Handlers
        private void строкаСостоянияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statusStrip.Visible = строкаСостоянияToolStripMenuItem.Checked)
                this.tabs.Height -= строкаСостоянияToolStripMenuItem.Height;
            else
                this.tabs.Height += строкаСостоянияToolStripMenuItem.Height;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about = string.Format("Текстовый редактор \"C# Text Editor\" v.1.0.\n" +
                                        "Copyright © 2014 Титов Н.А. (гр. МА-12-1) [nekit94-08@mail.ru].\nДля использования исключительно в учебных целях.");
            MessageBox.Show(this, about, "About \"C# Text Editor\"", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void переносПоСловамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox textField = GetCurrentTextField;

            bool modified = textField.Modified;

            if (переносПоСловамToolStripMenuItem.Checked)
                textField.WordWrap = true;
            else
                textField.WordWrap = false;

            textField.Modified = modified;
        }

        private void закрытьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage tab in tabs.TabPages)
            {
                tabs.SelectedTab = tab;
                foreach (Control ctrl in tab.Controls)
                    if (ctrl is ToolStrip)
                        foreach (ToolStripItem button in ((ToolStrip)(ctrl)).Items)
                            if (button is ToolStripButton)
                                button.PerformClick();
            }
        }

        private void cboOpenEncoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string newEncoding = ((ToolStripComboBox)sender).Text;

                RichTextBox textField = GetCurrentTextField;

                if (!Path.HasExtension(tabs.SelectedTab.Name))
                {
                    (((object[])textField.Tag)[1]) = cboOpenEncoding.SelectedIndex;
                    (((object[])textField.Tag)[0]) = cboOpenEncoding.Text;
                    return;
                }

                if (cboOpenEncoding.SelectedIndex != (int)(((object[])textField.Tag)[1]))
                {
                    if (MessageBox.Show(this, String.Format("Вы действительно хотите переоткрыть файл в кодировке\n{0}?", newEncoding), "Смена кодировки", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        cboOpenEncoding.SelectedIndex = (int)(((object[])textField.Tag)[1]);
                    else
                    {
                        (((object[])textField.Tag)[1]) = cboOpenEncoding.SelectedIndex;
                        try { OpenFile(tabs.SelectedTab, tabs.SelectedTab.Name, newEncoding); }
                        catch (Exception ex) { MessageBox.Show(this, ex.Message, "Ошибка открытия файла", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                        RichTextBox newTextField = GetCurrentTextField;

                        newTextField.Modified = false;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void шрифтToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog.ShowDialog() == DialogResult.Cancel)
                return;

            RichTextBox textField = GetCurrentTextField;

            bool modified = textField.Modified;

            textField.SelectionColor = fontDialog.Color;
            textField.SelectionFont = fontDialog.Font;

            textField.Modified = modified;
        }

        private void отменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox textField = GetCurrentTextField;

            if (textField.CanUndo)
                textField.Undo();
        }

        private void вернутьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox textField = GetCurrentTextField;

            if (textField.CanRedo)
                textField.Redo();
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentTextField.Cut();
        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentTextField.Copy();
        }

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentTextField.Paste();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox textField = GetCurrentTextField;

            if (textField.SelectedText == "")
                textField.Select(textField.SelectionStart, 1);

            textField.SelectedText = "";
        }

        private void найтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            search.Hide();
            search.Show(this);
            OnFindClick(GetCurrentTextField, new FindClickEventArgs());
        }

        private void найтиДалееToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OnFindClick(GetCurrentTextField, new FindClickEventArgs(true));
        }

        private void заменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormReplace(GetCurrentTextField).Show(this);
        }

        private void выбратьВсёToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentTextField.SelectAll();
        }

        private void времяИДатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dateTime = String.Format("{0:g}", DateTime.Now);

            GetCurrentTextField.SelectedText = dateTime;
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTab();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                foreach (string path in openFileDialog.FileNames)
                    AddTab(path);
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox textField = GetCurrentTextField;

                if (Path.HasExtension(tabs.SelectedTab.Name))
                {
                    try
                    {
                        File.WriteAllText(tabs.SelectedTab.Name, textField.Text, Encoding.GetEncoding(codePages[cboSaveEncoding.Text].Item1));
                        textField.Modified = false;
                    }
                    catch (Exception ex) { MessageBox.Show(this, ex.Message, "Ошибка при сохранении файла", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    return;
                }
                else
                {
                    saveFileDialog.FileName = "";
                    saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    saveFileDialog.Title = "Сохранить";

                    if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        if (File.Exists(saveFileDialog.FileName))
                        {
                            if (MessageBox.Show(this, "Файл с таким именем уже существует\nХотите переписать его?", "Сохранение файла", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                return;
                        }
                        try { File.WriteAllText(saveFileDialog.FileName, textField.Text, Encoding.GetEncoding(codePages[cboSaveEncoding.Text].Item1)); }
                        catch (Exception ex)
                        {
                            MessageBox.Show(this, ex.Message, "Ошибка при сохранении файла", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                        return;
                }

                TabPage tab = (TabPage)textField.Parent;

                окнаToolStripMenuItem.DropDownItems.RemoveByKey(tab.Name);

                tab.Name = saveFileDialog.FileName;
                tab.Text = Path.GetFileName(saveFileDialog.FileName);

                ToolStripMenuItem newItem = new ToolStripMenuItem(tab.Name, null, ToolStripMenuItemTabName_Click);
                newItem.CheckOnClick = true;
                newItem.Checked = true;
                newItem.Name = tab.Name;
                окнаToolStripMenuItem.DropDownItems.Add(newItem);

                OpenFile(tab, tab.Name);

                textField.Modified = false;
            }
            catch (Exception ex) { MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox textField = GetCurrentTextField;

                saveFileDialog.FileName = "";
                saveFileDialog.InitialDirectory = Path.HasExtension(tabs.SelectedTab.Name) ? Path.GetDirectoryName(tabs.SelectedTab.Name) : Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                saveFileDialog.Title = "Сохранить как";

                if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    if (File.Exists(saveFileDialog.FileName))
                    {
                        if (MessageBox.Show(this, "Файл с таким именем уже существует\nХотите переписать его?", "Сохранение файла", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                    }
                    try { File.WriteAllText(saveFileDialog.FileName, textField.Text, Encoding.GetEncoding(codePages[cboSaveEncoding.Text].Item1)); }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, ex.Message, "Ошибка при сохранении файла", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    return;

                textField.Modified = false;
                foreach (Control ctrl in tabs.SelectedTab.Controls)
                    if (ctrl is ToolStrip)
                        foreach (ToolStripItem button in ((ToolStrip)(ctrl)).Items)
                            if (button is ToolStripButton)
                                button.PerformClick();

                AddTab(saveFileDialog.FileName);
            }
            catch (Exception ex) { MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Context Menu Events Handlers
        private void отменитьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            отменитьToolStripMenuItem.PerformClick();
        }

        private void вернутьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            вернутьToolStripMenuItem.PerformClick();
        }

        private void вырезатьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            вырезатьToolStripMenuItem.PerformClick();
        }

        private void копироватьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            копироватьToolStripMenuItem.PerformClick();
        }

        private void вставитьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            вставитьToolStripMenuItem.PerformClick();
        }

        private void удалитьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            удалитьToolStripMenuItem.PerformClick();
        }

        private void найтиContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            найтиToolStripMenuItem.PerformClick();
        }

        private void найтиДалееContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            найтиДалееToolStripMenuItem.PerformClick();
        }

        private void заменитьContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            заменитьToolStripMenuItem.PerformClick();
        }

        private void выбратьContextВсеToolStripMenuIte_Click(object sender, EventArgs e)
        {
            выбратьВсёToolStripMenuItem.PerformClick();
        }

        private void времяИДатаContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            времяИДатаToolStripMenuItem.PerformClick();
        }
        #endregion
    }
}
