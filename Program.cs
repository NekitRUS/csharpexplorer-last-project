﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace CSharp_Explorer
{
    static class Program
    {
        private static Mutex oneApp;
        public static FormMain mainWindow;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                bool ok;
                oneApp = new Mutex(true, "OneInstance", out ok);
                if (!ok)
                {
                    MessageBox.Show("C# Explorer уже запущен!", "Запуск приложения", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                GC.KeepAlive(oneApp);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);
                Application.Run(mainWindow = new FormMain());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fatal error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
    }
}