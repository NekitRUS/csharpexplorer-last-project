﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CSharp_Explorer
{
    public partial class FormRenameOrCreate: Form
    {
        public enum FormOptions { Create, Rename };

        public FormRenameOrCreate(FormOptions options, string parentDirectoryNameOrOldName)
        {
            InitializeComponent();
            txtName.Tag = parentDirectoryNameOrOldName;
            if (options == FormOptions.Create)
                this.Text = "Новая папка";
            else
            {
                this.Text = "Переименовать";
                txtName.Text = Path.GetFileName(parentDirectoryNameOrOldName);
                txtName.SelectAll();
            }
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (this.Text == "Переименовать")
                Operations.Rename((string)txtName.Tag, txtName.Text);
            else
                Operations.CreateDirectory((string)txtName.Tag, txtName.Text);
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            throw new ArgumentNullException();
        }
    }
}
