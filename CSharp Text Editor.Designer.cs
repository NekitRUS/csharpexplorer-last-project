﻿namespace CSharp_Explorer
{
    partial class CSharp_Text_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CSharp_Text_Editor));
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьКакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вернутьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.вырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.найтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиДалееToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.выбратьВсёToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.времяИДатаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кодировкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переоткрытьВКодировкеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cboOpenEncoding = new System.Windows.Forms.ToolStripComboBox();
            this.кодировкаДляСохраненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cboSaveEncoding = new System.Windows.Forms.ToolStripComboBox();
            this.форматToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переносПоСловамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шрифтToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкаСостоянияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.окнаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьВсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.positionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabs = new System.Windows.Forms.TabControl();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.cmnuEdit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.отменитьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вернутьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.вырезатьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копироватьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставитьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.найтиContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиДалееContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заменитьContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.выбратьContextВсеToolStripMenuIte = new System.Windows.Forms.ToolStripMenuItem();
            this.времяИДатаContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mnuMain.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.cmnuEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.редактированиеToolStripMenuItem,
            this.кодировкаToolStripMenuItem,
            this.форматToolStripMenuItem,
            this.видToolStripMenuItem,
            this.окнаToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(584, 24);
            this.mnuMain.TabIndex = 0;
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.сохранитьКакToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // создатьToolStripMenuItem
            // 
            this.создатьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.add;
            this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
            this.создатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.создатьToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.создатьToolStripMenuItem.Text = "Создать";
            this.создатьToolStripMenuItem.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.open;
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.save;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // сохранитьКакToolStripMenuItem
            // 
            this.сохранитьКакToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.save_as;
            this.сохранитьКакToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
            this.сохранитьКакToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.сохранитьКакToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.сохранитьКакToolStripMenuItem.Text = "Сохранить как";
            this.сохранитьКакToolStripMenuItem.Click += new System.EventHandler(this.сохранитьКакToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(222, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.exit;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // редактированиеToolStripMenuItem
            // 
            this.редактированиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отменитьToolStripMenuItem,
            this.вернутьToolStripMenuItem,
            this.toolStripSeparator2,
            this.вырезатьToolStripMenuItem,
            this.копироватьToolStripMenuItem,
            this.вставитьToolStripMenuItem,
            this.удалитьToolStripMenuItem,
            this.toolStripSeparator3,
            this.найтиToolStripMenuItem,
            this.найтиДалееToolStripMenuItem,
            this.заменитьToolStripMenuItem,
            this.toolStripSeparator4,
            this.выбратьВсёToolStripMenuItem,
            this.времяИДатаToolStripMenuItem});
            this.редактированиеToolStripMenuItem.Name = "редактированиеToolStripMenuItem";
            this.редактированиеToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.редактированиеToolStripMenuItem.Text = "Редактирование";
            // 
            // отменитьToolStripMenuItem
            // 
            this.отменитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.undo;
            this.отменитьToolStripMenuItem.Name = "отменитьToolStripMenuItem";
            this.отменитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.отменитьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.отменитьToolStripMenuItem.Text = "Отменить";
            this.отменитьToolStripMenuItem.Click += new System.EventHandler(this.отменитьToolStripMenuItem_Click);
            // 
            // вернутьToolStripMenuItem
            // 
            this.вернутьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.redo;
            this.вернутьToolStripMenuItem.Name = "вернутьToolStripMenuItem";
            this.вернутьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.вернутьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вернутьToolStripMenuItem.Text = "Вернуть";
            this.вернутьToolStripMenuItem.Click += new System.EventHandler(this.вернутьToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(215, 6);
            // 
            // вырезатьToolStripMenuItem
            // 
            this.вырезатьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.cut;
            this.вырезатьToolStripMenuItem.Name = "вырезатьToolStripMenuItem";
            this.вырезатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.вырезатьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вырезатьToolStripMenuItem.Text = "Вырезать";
            this.вырезатьToolStripMenuItem.Click += new System.EventHandler(this.вырезатьToolStripMenuItem_Click);
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.copy;
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.копироватьToolStripMenuItem.Text = "Копировать";
            this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.копироватьToolStripMenuItem_Click);
            // 
            // вставитьToolStripMenuItem
            // 
            this.вставитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.paste;
            this.вставитьToolStripMenuItem.Name = "вставитьToolStripMenuItem";
            this.вставитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.вставитьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вставитьToolStripMenuItem.Text = "Вставить";
            this.вставитьToolStripMenuItem.Click += new System.EventHandler(this.вставитьToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.delete;
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(215, 6);
            // 
            // найтиToolStripMenuItem
            // 
            this.найтиToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.find;
            this.найтиToolStripMenuItem.Name = "найтиToolStripMenuItem";
            this.найтиToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.найтиToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.найтиToolStripMenuItem.Text = "Найти";
            this.найтиToolStripMenuItem.Click += new System.EventHandler(this.найтиToolStripMenuItem_Click);
            // 
            // найтиДалееToolStripMenuItem
            // 
            this.найтиДалееToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.search;
            this.найтиДалееToolStripMenuItem.Name = "найтиДалееToolStripMenuItem";
            this.найтиДалееToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.найтиДалееToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.найтиДалееToolStripMenuItem.Text = "Найти далее";
            this.найтиДалееToolStripMenuItem.Click += new System.EventHandler(this.найтиДалееToolStripMenuItem_Click);
            // 
            // заменитьToolStripMenuItem
            // 
            this.заменитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.replace;
            this.заменитьToolStripMenuItem.Name = "заменитьToolStripMenuItem";
            this.заменитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.заменитьToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.заменитьToolStripMenuItem.Text = "Заменить";
            this.заменитьToolStripMenuItem.Click += new System.EventHandler(this.заменитьToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(215, 6);
            // 
            // выбратьВсёToolStripMenuItem
            // 
            this.выбратьВсёToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.select_all;
            this.выбратьВсёToolStripMenuItem.Name = "выбратьВсёToolStripMenuItem";
            this.выбратьВсёToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.выбратьВсёToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.выбратьВсёToolStripMenuItem.Text = "Выбрать всё";
            this.выбратьВсёToolStripMenuItem.Click += new System.EventHandler(this.выбратьВсёToolStripMenuItem_Click);
            // 
            // времяИДатаToolStripMenuItem
            // 
            this.времяИДатаToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.date_time;
            this.времяИДатаToolStripMenuItem.Name = "времяИДатаToolStripMenuItem";
            this.времяИДатаToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.T)));
            this.времяИДатаToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.времяИДатаToolStripMenuItem.Text = "Время и дата";
            this.времяИДатаToolStripMenuItem.Click += new System.EventHandler(this.времяИДатаToolStripMenuItem_Click);
            // 
            // кодировкаToolStripMenuItem
            // 
            this.кодировкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.переоткрытьВКодировкеToolStripMenuItem,
            this.кодировкаДляСохраненияToolStripMenuItem});
            this.кодировкаToolStripMenuItem.Name = "кодировкаToolStripMenuItem";
            this.кодировкаToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.кодировкаToolStripMenuItem.Text = "Кодировка";
            // 
            // переоткрытьВКодировкеToolStripMenuItem
            // 
            this.переоткрытьВКодировкеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cboOpenEncoding});
            this.переоткрытьВКодировкеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.open_encoding;
            this.переоткрытьВКодировкеToolStripMenuItem.Name = "переоткрытьВКодировкеToolStripMenuItem";
            this.переоткрытьВКодировкеToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.переоткрытьВКодировкеToolStripMenuItem.Text = "Переоткрыть в кодировке";
            // 
            // cboOpenEncoding
            // 
            this.cboOpenEncoding.BackColor = System.Drawing.SystemColors.Window;
            this.cboOpenEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOpenEncoding.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cboOpenEncoding.Name = "cboOpenEncoding";
            this.cboOpenEncoding.Size = new System.Drawing.Size(200, 23);
            this.cboOpenEncoding.SelectedIndexChanged += new System.EventHandler(this.cboOpenEncoding_SelectedIndexChanged);
            // 
            // кодировкаДляСохраненияToolStripMenuItem
            // 
            this.кодировкаДляСохраненияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cboSaveEncoding});
            this.кодировкаДляСохраненияToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.save_encoding;
            this.кодировкаДляСохраненияToolStripMenuItem.Name = "кодировкаДляСохраненияToolStripMenuItem";
            this.кодировкаДляСохраненияToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.кодировкаДляСохраненияToolStripMenuItem.Text = "Кодировка для сохранения";
            // 
            // cboSaveEncoding
            // 
            this.cboSaveEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSaveEncoding.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cboSaveEncoding.Name = "cboSaveEncoding";
            this.cboSaveEncoding.Size = new System.Drawing.Size(200, 23);
            // 
            // форматToolStripMenuItem
            // 
            this.форматToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.переносПоСловамToolStripMenuItem,
            this.шрифтToolStripMenuItem});
            this.форматToolStripMenuItem.Name = "форматToolStripMenuItem";
            this.форматToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.форматToolStripMenuItem.Text = "Формат";
            // 
            // переносПоСловамToolStripMenuItem
            // 
            this.переносПоСловамToolStripMenuItem.CheckOnClick = true;
            this.переносПоСловамToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.wrap;
            this.переносПоСловамToolStripMenuItem.Name = "переносПоСловамToolStripMenuItem";
            this.переносПоСловамToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.переносПоСловамToolStripMenuItem.Text = "Перенос по словам";
            this.переносПоСловамToolStripMenuItem.Click += new System.EventHandler(this.переносПоСловамToolStripMenuItem_Click);
            // 
            // шрифтToolStripMenuItem
            // 
            this.шрифтToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.font;
            this.шрифтToolStripMenuItem.Name = "шрифтToolStripMenuItem";
            this.шрифтToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.шрифтToolStripMenuItem.Text = "Шрифт";
            this.шрифтToolStripMenuItem.Click += new System.EventHandler(this.шрифтToolStripMenuItem_Click);
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкаСостоянияToolStripMenuItem});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.видToolStripMenuItem.Text = "Вид";
            // 
            // строкаСостоянияToolStripMenuItem
            // 
            this.строкаСостоянияToolStripMenuItem.Checked = true;
            this.строкаСостоянияToolStripMenuItem.CheckOnClick = true;
            this.строкаСостоянияToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.строкаСостоянияToolStripMenuItem.Name = "строкаСостоянияToolStripMenuItem";
            this.строкаСостоянияToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.строкаСостоянияToolStripMenuItem.Text = "Строка состояния";
            this.строкаСостоянияToolStripMenuItem.Click += new System.EventHandler(this.строкаСостоянияToolStripMenuItem_Click);
            // 
            // окнаToolStripMenuItem
            // 
            this.окнаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.закрытьВсеToolStripMenuItem});
            this.окнаToolStripMenuItem.Name = "окнаToolStripMenuItem";
            this.окнаToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.окнаToolStripMenuItem.Text = "Окна";
            // 
            // закрытьВсеToolStripMenuItem
            // 
            this.закрытьВсеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.cross;
            this.закрытьВсеToolStripMenuItem.Name = "закрытьВсеToolStripMenuItem";
            this.закрытьВсеToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.закрытьВсеToolStripMenuItem.Text = "Закрыть все";
            this.закрытьВсеToolStripMenuItem.Click += new System.EventHandler(this.закрытьВсеToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.info;
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.positionLabel,
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 390);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(584, 22);
            this.statusStrip.TabIndex = 1;
            // 
            // positionLabel
            // 
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(284, 17);
            this.positionLabel.Spring = true;
            this.positionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(284, 17);
            this.statusLabel.Spring = true;
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabs
            // 
            this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabs.Location = new System.Drawing.Point(12, 27);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(560, 360);
            this.tabs.TabIndex = 2;
            this.tabs.SelectedIndexChanged += new System.EventHandler(this.tabs_SelectedIndexChanged);
            // 
            // fontDialog
            // 
            this.fontDialog.ShowColor = true;
            // 
            // cmnuEdit
            // 
            this.cmnuEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отменитьContextToolStripMenuItem,
            this.вернутьContextToolStripMenuItem,
            this.toolStripSeparator5,
            this.вырезатьContextToolStripMenuItem,
            this.копироватьContextToolStripMenuItem,
            this.вставитьContextToolStripMenuItem,
            this.удалитьContextToolStripMenuItem,
            this.toolStripSeparator6,
            this.найтиContextToolStripMenuItem,
            this.найтиДалееContextToolStripMenuItem,
            this.заменитьContextToolStripMenuItem,
            this.toolStripSeparator7,
            this.выбратьContextВсеToolStripMenuIte,
            this.времяИДатаContextToolStripMenuItem});
            this.cmnuEdit.Name = "cmnuEdit";
            this.cmnuEdit.Size = new System.Drawing.Size(219, 264);
            // 
            // отменитьContextToolStripMenuItem
            // 
            this.отменитьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.undo;
            this.отменитьContextToolStripMenuItem.Name = "отменитьContextToolStripMenuItem";
            this.отменитьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Z";
            this.отменитьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.отменитьContextToolStripMenuItem.Text = "Отменить";
            this.отменитьContextToolStripMenuItem.Click += new System.EventHandler(this.отменитьContextToolStripMenuItem_Click);
            // 
            // вернутьContextToolStripMenuItem
            // 
            this.вернутьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.redo;
            this.вернутьContextToolStripMenuItem.Name = "вернутьContextToolStripMenuItem";
            this.вернутьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Y";
            this.вернутьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вернутьContextToolStripMenuItem.Text = "Вернуть";
            this.вернутьContextToolStripMenuItem.Click += new System.EventHandler(this.вернутьContextToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(215, 6);
            // 
            // вырезатьContextToolStripMenuItem
            // 
            this.вырезатьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.cut;
            this.вырезатьContextToolStripMenuItem.Name = "вырезатьContextToolStripMenuItem";
            this.вырезатьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.вырезатьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вырезатьContextToolStripMenuItem.Text = "Вырезать";
            this.вырезатьContextToolStripMenuItem.Click += new System.EventHandler(this.вырезатьContextToolStripMenuItem_Click);
            // 
            // копироватьContextToolStripMenuItem
            // 
            this.копироватьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.copy;
            this.копироватьContextToolStripMenuItem.Name = "копироватьContextToolStripMenuItem";
            this.копироватьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.копироватьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.копироватьContextToolStripMenuItem.Text = "Копировать";
            this.копироватьContextToolStripMenuItem.Click += new System.EventHandler(this.копироватьContextToolStripMenuItem_Click);
            // 
            // вставитьContextToolStripMenuItem
            // 
            this.вставитьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.paste;
            this.вставитьContextToolStripMenuItem.Name = "вставитьContextToolStripMenuItem";
            this.вставитьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.вставитьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.вставитьContextToolStripMenuItem.Text = "Вставить";
            this.вставитьContextToolStripMenuItem.Click += new System.EventHandler(this.вставитьContextToolStripMenuItem_Click);
            // 
            // удалитьContextToolStripMenuItem
            // 
            this.удалитьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.delete;
            this.удалитьContextToolStripMenuItem.Name = "удалитьContextToolStripMenuItem";
            this.удалитьContextToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.удалитьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.удалитьContextToolStripMenuItem.Text = "Удалить";
            this.удалитьContextToolStripMenuItem.Click += new System.EventHandler(this.удалитьContextToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(215, 6);
            // 
            // найтиContextToolStripMenuItem
            // 
            this.найтиContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.find;
            this.найтиContextToolStripMenuItem.Name = "найтиContextToolStripMenuItem";
            this.найтиContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F";
            this.найтиContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.найтиContextToolStripMenuItem.Text = "Найти";
            this.найтиContextToolStripMenuItem.Click += new System.EventHandler(this.найтиContextToolStripMenuItem_Click);
            // 
            // найтиДалееContextToolStripMenuItem
            // 
            this.найтиДалееContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.search;
            this.найтиДалееContextToolStripMenuItem.Name = "найтиДалееContextToolStripMenuItem";
            this.найтиДалееContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+F";
            this.найтиДалееContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.найтиДалееContextToolStripMenuItem.Text = "Найти далее";
            this.найтиДалееContextToolStripMenuItem.Click += new System.EventHandler(this.найтиДалееContextToolStripMenuItem_Click);
            // 
            // заменитьContextToolStripMenuItem
            // 
            this.заменитьContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.replace;
            this.заменитьContextToolStripMenuItem.Name = "заменитьContextToolStripMenuItem";
            this.заменитьContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+H";
            this.заменитьContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.заменитьContextToolStripMenuItem.Text = "Заменить";
            this.заменитьContextToolStripMenuItem.Click += new System.EventHandler(this.заменитьContextToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(215, 6);
            // 
            // выбратьContextВсеToolStripMenuIte
            // 
            this.выбратьContextВсеToolStripMenuIte.Image = global::CSharp_Explorer.Properties.Resources.select_all;
            this.выбратьContextВсеToolStripMenuIte.Name = "выбратьContextВсеToolStripMenuIte";
            this.выбратьContextВсеToolStripMenuIte.ShortcutKeyDisplayString = "Ctrl+A";
            this.выбратьContextВсеToolStripMenuIte.Size = new System.Drawing.Size(218, 22);
            this.выбратьContextВсеToolStripMenuIte.Text = "Выбрать всё";
            this.выбратьContextВсеToolStripMenuIte.Click += new System.EventHandler(this.выбратьContextВсеToolStripMenuIte_Click);
            // 
            // времяИДатаContextToolStripMenuItem
            // 
            this.времяИДатаContextToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.date_time;
            this.времяИДатаContextToolStripMenuItem.Name = "времяИДатаContextToolStripMenuItem";
            this.времяИДатаContextToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+T";
            this.времяИДатаContextToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.времяИДатаContextToolStripMenuItem.Text = "Время и дата";
            this.времяИДатаContextToolStripMenuItem.Click += new System.EventHandler(this.времяИДатаContextToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы|*.*";
            this.openFileDialog.Multiselect = true;
            this.openFileDialog.Title = "Открыть";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы|*.*";
            this.saveFileDialog.FilterIndex = 2;
            this.saveFileDialog.OverwritePrompt = false;
            this.saveFileDialog.Title = "Сохранить";
            // 
            // CSharp_Text_Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 412);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mnuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnuMain;
            this.MinimumSize = new System.Drawing.Size(600, 450);
            this.Name = "CSharp_Text_Editor";
            this.Text = "CSharp Text Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CSharp_Text_Editor_FormClosing);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.cmnuEdit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel positionLabel;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кодировкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem форматToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem окнаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьКакToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отменитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вернутьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem найтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиДалееToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заменитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem выбратьВсёToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem времяИДатаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переносПоСловамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шрифтToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкаСостоянияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьВсеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.ContextMenuStrip cmnuEdit;
        private System.Windows.Forms.ToolStripMenuItem отменитьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вернутьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem вырезатьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставитьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem найтиContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиДалееContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заменитьContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem выбратьContextВсеToolStripMenuIte;
        private System.Windows.Forms.ToolStripMenuItem времяИДатаContextToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem переоткрытьВКодировкеToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cboOpenEncoding;
        private System.Windows.Forms.ToolStripMenuItem кодировкаДляСохраненияToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cboSaveEncoding;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    }
}