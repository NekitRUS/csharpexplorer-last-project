﻿namespace CSharp_Explorer
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьВНовомОкнеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.архивироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.новаяПапкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.свойстваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переименоватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.переместитьВПапкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скопироватьВПапкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.выделитьВсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвертироватьВыделениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.найтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текущийВидToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.большиеИконкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таблицаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.маленькиеИконкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.плиткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.сортироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поВозрастаниюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поУбываниюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.показыватьФайлыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скрытыеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.системныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.строкаСостоянияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сервисToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текстовыйРедакторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelDetails = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.ilistTree = new System.Windows.Forms.ImageList(this.components);
            this.cmdClear = new System.Windows.Forms.Button();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lstvBrowser = new System.Windows.Forms.ListView();
            this.contextMenuFolder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextвыделитьВсёToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextинвертироватьВыделениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextвставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextнайтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextтекущийВидToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextбольшиеИконкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextтаблицаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextмаленькиеИконкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextсписокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextплиткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextоткрытьВНовомОкнеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextархивироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextкопироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextвырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextудалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ilistViewLarge = new System.Windows.Forms.ImageList(this.components);
            this.ilistViewSmall = new System.Windows.Forms.ImageList(this.components);
            this.treeviewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьВИзбранноеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьИзИзбранноеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.contextMenuFolder.SuspendLayout();
            this.treeviewMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.редактированиеToolStripMenuItem,
            this.видToolStripMenuItem,
            this.сервисToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(864, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьВНовомОкнеToolStripMenuItem,
            this.архивироватьToolStripMenuItem,
            this.toolStripSeparator1,
            this.новаяПапкаToolStripMenuItem,
            this.toolStripSeparator7,
            this.свойстваToolStripMenuItem,
            this.toolStripSeparator6,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "&Файл";
            // 
            // открытьВНовомОкнеToolStripMenuItem
            // 
            this.открытьВНовомОкнеToolStripMenuItem.Enabled = false;
            this.открытьВНовомОкнеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.open;
            this.открытьВНовомОкнеToolStripMenuItem.Name = "открытьВНовомОкнеToolStripMenuItem";
            this.открытьВНовомОкнеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьВНовомОкнеToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.открытьВНовомОкнеToolStripMenuItem.Text = "Открыть в новом окне";
            this.открытьВНовомОкнеToolStripMenuItem.Click += new System.EventHandler(this.открытьВНовомОкнеToolStripMenuItem_Click);
            // 
            // архивироватьToolStripMenuItem
            // 
            this.архивироватьToolStripMenuItem.Enabled = false;
            this.архивироватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.archive;
            this.архивироватьToolStripMenuItem.Name = "архивироватьToolStripMenuItem";
            this.архивироватьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.архивироватьToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.архивироватьToolStripMenuItem.Text = "Архивировать";
            this.архивироватьToolStripMenuItem.Click += new System.EventHandler(this.архивироватьToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(238, 6);
            // 
            // новаяПапкаToolStripMenuItem
            // 
            this.новаяПапкаToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.new_folder;
            this.новаяПапкаToolStripMenuItem.Name = "новаяПапкаToolStripMenuItem";
            this.новаяПапкаToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.новаяПапкаToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.новаяПапкаToolStripMenuItem.Text = "Новая папка";
            this.новаяПапкаToolStripMenuItem.Click += new System.EventHandler(this.новаяПапкаToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(238, 6);
            // 
            // свойстваToolStripMenuItem
            // 
            this.свойстваToolStripMenuItem.Enabled = false;
            this.свойстваToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.properties;
            this.свойстваToolStripMenuItem.Name = "свойстваToolStripMenuItem";
            this.свойстваToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.свойстваToolStripMenuItem.Text = "Свойства";
            this.свойстваToolStripMenuItem.Click += new System.EventHandler(this.свойстваToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(238, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.exit;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // редактированиеToolStripMenuItem
            // 
            this.редактированиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.переименоватьToolStripMenuItem,
            this.toolStripSeparator8,
            this.удалитьToolStripMenuItem,
            this.toolStripSeparator12,
            this.копироватьToolStripMenuItem,
            this.вырезатьToolStripMenuItem,
            this.вставитьToolStripMenuItem,
            this.toolStripSeparator2,
            this.переместитьВПапкуToolStripMenuItem,
            this.скопироватьВПапкуToolStripMenuItem,
            this.toolStripSeparator3,
            this.выделитьВсеToolStripMenuItem,
            this.инвертироватьВыделениеToolStripMenuItem,
            this.toolStripSeparator4,
            this.найтиToolStripMenuItem});
            this.редактированиеToolStripMenuItem.Name = "редактированиеToolStripMenuItem";
            this.редактированиеToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.редактированиеToolStripMenuItem.Text = "&Редактирование";
            // 
            // переименоватьToolStripMenuItem
            // 
            this.переименоватьToolStripMenuItem.Enabled = false;
            this.переименоватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.rename;
            this.переименоватьToolStripMenuItem.Name = "переименоватьToolStripMenuItem";
            this.переименоватьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.переименоватьToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.переименоватьToolStripMenuItem.Text = "Переименовать";
            this.переименоватьToolStripMenuItem.Click += new System.EventHandler(this.переименоватьToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(264, 6);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Enabled = false;
            this.удалитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.delete;
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(264, 6);
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Enabled = false;
            this.копироватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.copy;
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.копироватьToolStripMenuItem.Text = "Копировать";
            this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.копироватьToolStripMenuItem_Click);
            // 
            // вырезатьToolStripMenuItem
            // 
            this.вырезатьToolStripMenuItem.Enabled = false;
            this.вырезатьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.cut;
            this.вырезатьToolStripMenuItem.Name = "вырезатьToolStripMenuItem";
            this.вырезатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.вырезатьToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.вырезатьToolStripMenuItem.Text = "Вырезать";
            this.вырезатьToolStripMenuItem.Click += new System.EventHandler(this.вырезатьToolStripMenuItem_Click);
            // 
            // вставитьToolStripMenuItem
            // 
            this.вставитьToolStripMenuItem.Enabled = false;
            this.вставитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.paste;
            this.вставитьToolStripMenuItem.Name = "вставитьToolStripMenuItem";
            this.вставитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.вставитьToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.вставитьToolStripMenuItem.Text = "Вставить";
            this.вставитьToolStripMenuItem.Click += new System.EventHandler(this.вставитьToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(264, 6);
            // 
            // переместитьВПапкуToolStripMenuItem
            // 
            this.переместитьВПапкуToolStripMenuItem.Name = "переместитьВПапкуToolStripMenuItem";
            this.переместитьВПапкуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.M)));
            this.переместитьВПапкуToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.переместитьВПапкуToolStripMenuItem.Text = "Переместить в папку";
            this.переместитьВПапкуToolStripMenuItem.Click += new System.EventHandler(this.переместитьВПапкуToolStripMenuItem_Click);
            // 
            // скопироватьВПапкуToolStripMenuItem
            // 
            this.скопироватьВПапкуToolStripMenuItem.Name = "скопироватьВПапкуToolStripMenuItem";
            this.скопироватьВПапкуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.скопироватьВПапкуToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.скопироватьВПапкуToolStripMenuItem.Text = "Скопировать в папку";
            this.скопироватьВПапкуToolStripMenuItem.Click += new System.EventHandler(this.скопироватьВПапкуToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(264, 6);
            // 
            // выделитьВсеToolStripMenuItem
            // 
            this.выделитьВсеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.select_all;
            this.выделитьВсеToolStripMenuItem.Name = "выделитьВсеToolStripMenuItem";
            this.выделитьВсеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.выделитьВсеToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.выделитьВсеToolStripMenuItem.Text = "Выделить все";
            this.выделитьВсеToolStripMenuItem.Click += new System.EventHandler(this.выделитьВсеToolStripMenuItem_Click);
            // 
            // инвертироватьВыделениеToolStripMenuItem
            // 
            this.инвертироватьВыделениеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.deselect;
            this.инвертироватьВыделениеToolStripMenuItem.Name = "инвертироватьВыделениеToolStripMenuItem";
            this.инвертироватьВыделениеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.инвертироватьВыделениеToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.инвертироватьВыделениеToolStripMenuItem.Text = "Инвертировать выделение";
            this.инвертироватьВыделениеToolStripMenuItem.Click += new System.EventHandler(this.инвертироватьВыделениеToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(264, 6);
            // 
            // найтиToolStripMenuItem
            // 
            this.найтиToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.find;
            this.найтиToolStripMenuItem.Name = "найтиToolStripMenuItem";
            this.найтиToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.найтиToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.найтиToolStripMenuItem.Text = "Найти";
            this.найтиToolStripMenuItem.Click += new System.EventHandler(this.найтиToolStripMenuItem_Click);
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.текущийВидToolStripMenuItem,
            this.toolStripSeparator9,
            this.сортироватьToolStripMenuItem,
            this.toolStripSeparator10,
            this.показыватьФайлыToolStripMenuItem,
            this.toolStripSeparator11,
            this.строкаСостоянияToolStripMenuItem,
            this.toolStripSeparator5,
            this.обновитьToolStripMenuItem});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.видToolStripMenuItem.Text = "&Вид";
            // 
            // текущийВидToolStripMenuItem
            // 
            this.текущийВидToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.большиеИконкиToolStripMenuItem,
            this.таблицаToolStripMenuItem,
            this.маленькиеИконкиToolStripMenuItem,
            this.списокToolStripMenuItem,
            this.плиткаToolStripMenuItem});
            this.текущийВидToolStripMenuItem.Name = "текущийВидToolStripMenuItem";
            this.текущийВидToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.текущийВидToolStripMenuItem.Text = "Текущий вид";
            // 
            // большиеИконкиToolStripMenuItem
            // 
            this.большиеИконкиToolStripMenuItem.CheckOnClick = true;
            this.большиеИконкиToolStripMenuItem.Name = "большиеИконкиToolStripMenuItem";
            this.большиеИконкиToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.большиеИконкиToolStripMenuItem.Text = "Большие иконки";
            this.большиеИконкиToolStripMenuItem.Click += new System.EventHandler(this.большиеИконкиToolStripMenuItem_Click);
            // 
            // таблицаToolStripMenuItem
            // 
            this.таблицаToolStripMenuItem.Checked = true;
            this.таблицаToolStripMenuItem.CheckOnClick = true;
            this.таблицаToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.таблицаToolStripMenuItem.Name = "таблицаToolStripMenuItem";
            this.таблицаToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.таблицаToolStripMenuItem.Text = "Таблица";
            this.таблицаToolStripMenuItem.Click += new System.EventHandler(this.таблицаToolStripMenuItem_Click);
            // 
            // маленькиеИконкиToolStripMenuItem
            // 
            this.маленькиеИконкиToolStripMenuItem.CheckOnClick = true;
            this.маленькиеИконкиToolStripMenuItem.Name = "маленькиеИконкиToolStripMenuItem";
            this.маленькиеИконкиToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.маленькиеИконкиToolStripMenuItem.Text = "Маленькие иконки";
            this.маленькиеИконкиToolStripMenuItem.Click += new System.EventHandler(this.маленькиеИконкиToolStripMenuItem_Click);
            // 
            // списокToolStripMenuItem
            // 
            this.списокToolStripMenuItem.CheckOnClick = true;
            this.списокToolStripMenuItem.Name = "списокToolStripMenuItem";
            this.списокToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.списокToolStripMenuItem.Text = "Список";
            this.списокToolStripMenuItem.Click += new System.EventHandler(this.списокToolStripMenuItem_Click);
            // 
            // плиткаToolStripMenuItem
            // 
            this.плиткаToolStripMenuItem.CheckOnClick = true;
            this.плиткаToolStripMenuItem.Name = "плиткаToolStripMenuItem";
            this.плиткаToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.плиткаToolStripMenuItem.Text = "Плитка";
            this.плиткаToolStripMenuItem.Click += new System.EventHandler(this.плиткаToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(243, 6);
            // 
            // сортироватьToolStripMenuItem
            // 
            this.сортироватьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поВозрастаниюToolStripMenuItem,
            this.поУбываниюToolStripMenuItem});
            this.сортироватьToolStripMenuItem.Name = "сортироватьToolStripMenuItem";
            this.сортироватьToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.сортироватьToolStripMenuItem.Text = "Сортировать";
            // 
            // поВозрастаниюToolStripMenuItem
            // 
            this.поВозрастаниюToolStripMenuItem.CheckOnClick = true;
            this.поВозрастаниюToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.ascending;
            this.поВозрастаниюToolStripMenuItem.Name = "поВозрастаниюToolStripMenuItem";
            this.поВозрастаниюToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.поВозрастаниюToolStripMenuItem.Text = "По возрастанию";
            this.поВозрастаниюToolStripMenuItem.Click += new System.EventHandler(this.поВозрастаниюToolStripMenuItem_Click);
            // 
            // поУбываниюToolStripMenuItem
            // 
            this.поУбываниюToolStripMenuItem.CheckOnClick = true;
            this.поУбываниюToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.descending;
            this.поУбываниюToolStripMenuItem.Name = "поУбываниюToolStripMenuItem";
            this.поУбываниюToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.поУбываниюToolStripMenuItem.Text = "По убыванию";
            this.поУбываниюToolStripMenuItem.Click += new System.EventHandler(this.поУбываниюToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(243, 6);
            // 
            // показыватьФайлыToolStripMenuItem
            // 
            this.показыватьФайлыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.скрытыеToolStripMenuItem,
            this.системныеToolStripMenuItem});
            this.показыватьФайлыToolStripMenuItem.Name = "показыватьФайлыToolStripMenuItem";
            this.показыватьФайлыToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.показыватьФайлыToolStripMenuItem.Text = "Показывать файлы";
            // 
            // скрытыеToolStripMenuItem
            // 
            this.скрытыеToolStripMenuItem.CheckOnClick = true;
            this.скрытыеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.hidden;
            this.скрытыеToolStripMenuItem.Name = "скрытыеToolStripMenuItem";
            this.скрытыеToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.скрытыеToolStripMenuItem.Text = "Скрытые";
            this.скрытыеToolStripMenuItem.Click += new System.EventHandler(this.скрытыеToolStripMenuItem_Click);
            // 
            // системныеToolStripMenuItem
            // 
            this.системныеToolStripMenuItem.CheckOnClick = true;
            this.системныеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.system;
            this.системныеToolStripMenuItem.Name = "системныеToolStripMenuItem";
            this.системныеToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.системныеToolStripMenuItem.Text = "Системные";
            this.системныеToolStripMenuItem.Click += new System.EventHandler(this.системныеToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(243, 6);
            // 
            // строкаСостоянияToolStripMenuItem
            // 
            this.строкаСостоянияToolStripMenuItem.Checked = true;
            this.строкаСостоянияToolStripMenuItem.CheckOnClick = true;
            this.строкаСостоянияToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.строкаСостоянияToolStripMenuItem.Name = "строкаСостоянияToolStripMenuItem";
            this.строкаСостоянияToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.строкаСостоянияToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.строкаСостоянияToolStripMenuItem.Text = "Строка состояния";
            this.строкаСостоянияToolStripMenuItem.Click += new System.EventHandler(this.строкаСостоянияToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(243, 6);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.refresh;
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // сервисToolStripMenuItem
            // 
            this.сервисToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.текстовыйРедакторToolStripMenuItem});
            this.сервисToolStripMenuItem.Name = "сервисToolStripMenuItem";
            this.сервисToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.сервисToolStripMenuItem.Text = "&Сервис";
            // 
            // текстовыйРедакторToolStripMenuItem
            // 
            this.текстовыйРедакторToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.text_editor;
            this.текстовыйРедакторToolStripMenuItem.Name = "текстовыйРедакторToolStripMenuItem";
            this.текстовыйРедакторToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.T)));
            this.текстовыйРедакторToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.текстовыйРедакторToolStripMenuItem.Text = "Текстовый редактор";
            this.текстовыйРедакторToolStripMenuItem.Click += new System.EventHandler(this.текстовыйРедакторToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.помощьToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "С&правка";
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.помощьToolStripMenuItem.Text = "Помощь";
            this.помощьToolStripMenuItem.Click += new System.EventHandler(this.помощьToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.info;
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.statusLabelDetails});
            this.statusStrip.Location = new System.Drawing.Point(0, 380);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(864, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusLabelDetails
            // 
            this.statusLabelDetails.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusLabelDetails.Name = "statusLabelDetails";
            this.statusLabelDetails.Size = new System.Drawing.Size(849, 17);
            this.statusLabelDetails.Spring = true;
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(12, 27);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            this.splitContainer.Panel1MinSize = 230;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.cmdClear);
            this.splitContainer.Panel2.Controls.Add(this.cmdSearch);
            this.splitContainer.Panel2.Controls.Add(this.txtSearch);
            this.splitContainer.Panel2.Controls.Add(this.txtPath);
            this.splitContainer.Panel2.Controls.Add(this.lstvBrowser);
            this.splitContainer.Panel2.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.splitContainer.Panel2MinSize = 250;
            this.splitContainer.Size = new System.Drawing.Size(840, 350);
            this.splitContainer.SplitterDistance = 230;
            this.splitContainer.TabIndex = 3;
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.HideSelection = false;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.ilistTree;
            this.treeView.Location = new System.Drawing.Point(3, 3);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.ShowNodeToolTips = true;
            this.treeView.Size = new System.Drawing.Size(224, 344);
            this.treeView.TabIndex = 0;
            this.treeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterExpand);
            this.treeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeSelect);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            this.treeView.MouseLeave += new System.EventHandler(this.treeView_MouseLeave);
            // 
            // ilistTree
            // 
            this.ilistTree.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilistTree.ImageSize = new System.Drawing.Size(24, 24);
            this.ilistTree.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdClear.BackColor = System.Drawing.Color.White;
            this.cmdClear.BackgroundImage = global::CSharp_Explorer.Properties.Resources.cross;
            this.cmdClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cmdClear.Enabled = false;
            this.cmdClear.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cmdClear.FlatAppearance.BorderSize = 0;
            this.cmdClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.cmdClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClear.Location = new System.Drawing.Point(564, 10);
            this.cmdClear.Margin = new System.Windows.Forms.Padding(3, 3, 5, 3);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(10, 10);
            this.cmdClear.TabIndex = 8;
            this.cmdClear.UseVisualStyleBackColor = true;
            this.cmdClear.Visible = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSearch.Image = global::CSharp_Explorer.Properties.Resources.find;
            this.cmdSearch.Location = new System.Drawing.Point(579, 3);
            this.cmdSearch.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(24, 24);
            this.cmdSearch.TabIndex = 7;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSearch.ForeColor = System.Drawing.Color.Gray;
            this.txtSearch.Location = new System.Drawing.Point(388, 3);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(3, 3, 1, 3);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(190, 24);
            this.txtSearch.TabIndex = 6;
            this.txtSearch.Text = "Поиск в директории...";
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            this.txtSearch.Leave += new System.EventHandler(this.txtSearch_Leave);
            this.txtSearch.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtSearch_PreviewKeyDown);
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPath.Location = new System.Drawing.Point(6, 3);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(376, 24);
            this.txtPath.TabIndex = 5;
            this.txtPath.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPath_KeyUp);
            this.txtPath.Leave += new System.EventHandler(this.txtPath_Leave);
            this.txtPath.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPath_PreviewKeyDown);
            // 
            // lstvBrowser
            // 
            this.lstvBrowser.AllowDrop = true;
            this.lstvBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstvBrowser.ContextMenuStrip = this.contextMenuFolder;
            this.lstvBrowser.FullRowSelect = true;
            this.lstvBrowser.HideSelection = false;
            this.lstvBrowser.LargeImageList = this.ilistViewLarge;
            this.lstvBrowser.Location = new System.Drawing.Point(6, 33);
            this.lstvBrowser.Name = "lstvBrowser";
            this.lstvBrowser.Size = new System.Drawing.Size(600, 314);
            this.lstvBrowser.SmallImageList = this.ilistViewSmall;
            this.lstvBrowser.TabIndex = 1;
            this.lstvBrowser.UseCompatibleStateImageBehavior = false;
            this.lstvBrowser.View = System.Windows.Forms.View.Details;
            this.lstvBrowser.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstvBrowser_ColumnClick);
            this.lstvBrowser.ItemActivate += new System.EventHandler(this.lstvBrowser_ItemActivate);
            this.lstvBrowser.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstvBrowser_ItemSelectionChanged);
            this.lstvBrowser.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstvBrowser_DragDrop);
            this.lstvBrowser.DragOver += new System.Windows.Forms.DragEventHandler(this.lstvBrowser_DragOver);
            this.lstvBrowser.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lstvBrowser_MouseMove);
            this.lstvBrowser.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lstvBrowser_MouseUp);
            // 
            // contextMenuFolder
            // 
            this.contextMenuFolder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextвыделитьВсёToolStripMenuItem,
            this.ContextинвертироватьВыделениеToolStripMenuItem,
            this.toolStripSeparator13,
            this.ContextвставитьToolStripMenuItem,
            this.toolStripSeparator14,
            this.ContextнайтиToolStripMenuItem,
            this.toolStripSeparator15,
            this.ContextтекущийВидToolStripMenuItem,
            this.ContextоткрытьВНовомОкнеToolStripMenuItem,
            this.ContextархивироватьToolStripMenuItem,
            this.toolStripSeparator16,
            this.ContextкопироватьToolStripMenuItem,
            this.ContextвырезатьToolStripMenuItem,
            this.ContextудалитьToolStripMenuItem});
            this.contextMenuFolder.Name = "contextMenuFolder";
            this.contextMenuFolder.Size = new System.Drawing.Size(259, 248);
            this.contextMenuFolder.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuFolder_Opening);
            // 
            // ContextвыделитьВсёToolStripMenuItem
            // 
            this.ContextвыделитьВсёToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.select_all;
            this.ContextвыделитьВсёToolStripMenuItem.Name = "ContextвыделитьВсёToolStripMenuItem";
            this.ContextвыделитьВсёToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.ContextвыделитьВсёToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextвыделитьВсёToolStripMenuItem.Text = "Выделить всё";
            this.ContextвыделитьВсёToolStripMenuItem.Click += new System.EventHandler(this.ContextвыделитьВсёToolStripMenuItem_Click);
            // 
            // ContextинвертироватьВыделениеToolStripMenuItem
            // 
            this.ContextинвертироватьВыделениеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.deselect;
            this.ContextинвертироватьВыделениеToolStripMenuItem.Name = "ContextинвертироватьВыделениеToolStripMenuItem";
            this.ContextинвертироватьВыделениеToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+I";
            this.ContextинвертироватьВыделениеToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextинвертироватьВыделениеToolStripMenuItem.Text = "Инвертировать выделение";
            this.ContextинвертироватьВыделениеToolStripMenuItem.Click += new System.EventHandler(this.ContextинвертироватьВыделениеToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(255, 6);
            // 
            // ContextвставитьToolStripMenuItem
            // 
            this.ContextвставитьToolStripMenuItem.Enabled = false;
            this.ContextвставитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.paste;
            this.ContextвставитьToolStripMenuItem.Name = "ContextвставитьToolStripMenuItem";
            this.ContextвставитьToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.ContextвставитьToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextвставитьToolStripMenuItem.Text = "Вставить";
            this.ContextвставитьToolStripMenuItem.Click += new System.EventHandler(this.ContextвставитьToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(255, 6);
            // 
            // ContextнайтиToolStripMenuItem
            // 
            this.ContextнайтиToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.find;
            this.ContextнайтиToolStripMenuItem.Name = "ContextнайтиToolStripMenuItem";
            this.ContextнайтиToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F";
            this.ContextнайтиToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextнайтиToolStripMenuItem.Text = "Найти";
            this.ContextнайтиToolStripMenuItem.Click += new System.EventHandler(this.ContextнайтиToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(255, 6);
            // 
            // ContextтекущийВидToolStripMenuItem
            // 
            this.ContextтекущийВидToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextбольшиеИконкиToolStripMenuItem,
            this.ContextтаблицаToolStripMenuItem,
            this.ContextмаленькиеИконкиToolStripMenuItem,
            this.ContextсписокToolStripMenuItem,
            this.ContextплиткаToolStripMenuItem});
            this.ContextтекущийВидToolStripMenuItem.Name = "ContextтекущийВидToolStripMenuItem";
            this.ContextтекущийВидToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextтекущийВидToolStripMenuItem.Text = "Текущий вид";
            // 
            // ContextбольшиеИконкиToolStripMenuItem
            // 
            this.ContextбольшиеИконкиToolStripMenuItem.CheckOnClick = true;
            this.ContextбольшиеИконкиToolStripMenuItem.Name = "ContextбольшиеИконкиToolStripMenuItem";
            this.ContextбольшиеИконкиToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.ContextбольшиеИконкиToolStripMenuItem.Text = "Большие иконки";
            this.ContextбольшиеИконкиToolStripMenuItem.Click += new System.EventHandler(this.ContextбольшиеИконкиToolStripMenuItem_Click);
            // 
            // ContextтаблицаToolStripMenuItem
            // 
            this.ContextтаблицаToolStripMenuItem.Checked = true;
            this.ContextтаблицаToolStripMenuItem.CheckOnClick = true;
            this.ContextтаблицаToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ContextтаблицаToolStripMenuItem.Name = "ContextтаблицаToolStripMenuItem";
            this.ContextтаблицаToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.ContextтаблицаToolStripMenuItem.Text = "Таблица";
            this.ContextтаблицаToolStripMenuItem.Click += new System.EventHandler(this.ContextтаблицаToolStripMenuItem_Click);
            // 
            // ContextмаленькиеИконкиToolStripMenuItem
            // 
            this.ContextмаленькиеИконкиToolStripMenuItem.CheckOnClick = true;
            this.ContextмаленькиеИконкиToolStripMenuItem.Name = "ContextмаленькиеИконкиToolStripMenuItem";
            this.ContextмаленькиеИконкиToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.ContextмаленькиеИконкиToolStripMenuItem.Text = "Маленькие иконки";
            this.ContextмаленькиеИконкиToolStripMenuItem.Click += new System.EventHandler(this.ContextмаленькиеИконкиToolStripMenuItem_Click);
            // 
            // ContextсписокToolStripMenuItem
            // 
            this.ContextсписокToolStripMenuItem.CheckOnClick = true;
            this.ContextсписокToolStripMenuItem.Name = "ContextсписокToolStripMenuItem";
            this.ContextсписокToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.ContextсписокToolStripMenuItem.Text = "Список";
            this.ContextсписокToolStripMenuItem.Click += new System.EventHandler(this.ContextсписокToolStripMenuItem_Click);
            // 
            // ContextплиткаToolStripMenuItem
            // 
            this.ContextплиткаToolStripMenuItem.CheckOnClick = true;
            this.ContextплиткаToolStripMenuItem.Name = "ContextплиткаToolStripMenuItem";
            this.ContextплиткаToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.ContextплиткаToolStripMenuItem.Text = "Плитка";
            this.ContextплиткаToolStripMenuItem.Click += new System.EventHandler(this.ContextплиткаToolStripMenuItem_Click);
            // 
            // ContextоткрытьВНовомОкнеToolStripMenuItem
            // 
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.open;
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.Name = "ContextоткрытьВНовомОкнеToolStripMenuItem";
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+O";
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.Text = "Открыть в новом окне";
            this.ContextоткрытьВНовомОкнеToolStripMenuItem.Click += new System.EventHandler(this.ContextоткрытьВНовомОкнеToolStripMenuItem_Click);
            // 
            // ContextархивироватьToolStripMenuItem
            // 
            this.ContextархивироватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.archive;
            this.ContextархивироватьToolStripMenuItem.Name = "ContextархивироватьToolStripMenuItem";
            this.ContextархивироватьToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+P";
            this.ContextархивироватьToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextархивироватьToolStripMenuItem.Text = "Архивировать";
            this.ContextархивироватьToolStripMenuItem.Click += new System.EventHandler(this.ContextархивироватьToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(255, 6);
            // 
            // ContextкопироватьToolStripMenuItem
            // 
            this.ContextкопироватьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.copy;
            this.ContextкопироватьToolStripMenuItem.Name = "ContextкопироватьToolStripMenuItem";
            this.ContextкопироватьToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.ContextкопироватьToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextкопироватьToolStripMenuItem.Text = "Копировать";
            this.ContextкопироватьToolStripMenuItem.Click += new System.EventHandler(this.ContextкопироватьToolStripMenuItem_Click);
            // 
            // ContextвырезатьToolStripMenuItem
            // 
            this.ContextвырезатьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.cut;
            this.ContextвырезатьToolStripMenuItem.Name = "ContextвырезатьToolStripMenuItem";
            this.ContextвырезатьToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.ContextвырезатьToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextвырезатьToolStripMenuItem.Text = "Вырезать";
            this.ContextвырезатьToolStripMenuItem.Click += new System.EventHandler(this.ContextвырезатьToolStripMenuItem_Click);
            // 
            // ContextудалитьToolStripMenuItem
            // 
            this.ContextудалитьToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.delete;
            this.ContextудалитьToolStripMenuItem.Name = "ContextудалитьToolStripMenuItem";
            this.ContextудалитьToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.ContextудалитьToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.ContextудалитьToolStripMenuItem.Text = "Удалить";
            this.ContextудалитьToolStripMenuItem.Click += new System.EventHandler(this.ContextудалитьToolStripMenuItem_Click);
            // 
            // ilistViewLarge
            // 
            this.ilistViewLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilistViewLarge.ImageSize = new System.Drawing.Size(48, 48);
            this.ilistViewLarge.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ilistViewSmall
            // 
            this.ilistViewSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilistViewSmall.ImageSize = new System.Drawing.Size(24, 24);
            this.ilistViewSmall.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // treeviewMenu
            // 
            this.treeviewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьВИзбранноеToolStripMenuItem,
            this.удалитьИзИзбранноеToolStripMenuItem});
            this.treeviewMenu.Name = "contextMenuStrip1";
            this.treeviewMenu.Size = new System.Drawing.Size(210, 48);
            this.treeviewMenu.Opening += new System.ComponentModel.CancelEventHandler(this.treeviewMenu_Opening);
            // 
            // добавитьВИзбранноеToolStripMenuItem
            // 
            this.добавитьВИзбранноеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.add_favorite;
            this.добавитьВИзбранноеToolStripMenuItem.Name = "добавитьВИзбранноеToolStripMenuItem";
            this.добавитьВИзбранноеToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.добавитьВИзбранноеToolStripMenuItem.Text = "Добавить в \"Избранное\"";
            this.добавитьВИзбранноеToolStripMenuItem.Visible = false;
            this.добавитьВИзбранноеToolStripMenuItem.Click += new System.EventHandler(this.добавитьВИзбранноеToolStripMenuItem_Click);
            // 
            // удалитьИзИзбранноеToolStripMenuItem
            // 
            this.удалитьИзИзбранноеToolStripMenuItem.Image = global::CSharp_Explorer.Properties.Resources.remove_favorite;
            this.удалитьИзИзбранноеToolStripMenuItem.Name = "удалитьИзИзбранноеToolStripMenuItem";
            this.удалитьИзИзбранноеToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.удалитьИзИзбранноеToolStripMenuItem.Text = "Удалить из \"Избранное\"";
            this.удалитьИзИзбранноеToolStripMenuItem.Visible = false;
            this.удалитьИзИзбранноеToolStripMenuItem.Click += new System.EventHandler(this.удалитьИзИзбранноеToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 402);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(880, 440);
            this.Name = "FormMain";
            this.Text = "C# Explorer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.contextMenuFolder.ResumeLayout(false);
            this.treeviewMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ListView lstvBrowser;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сервисToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьВНовомОкнеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem архивироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem переместитьВПапкуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скопироватьВПапкуToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem выделитьВсеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инвертироватьВыделениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem найтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текущийВидToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сортироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показыватьФайлыToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem строкаСостоянияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текстовыйРедакторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяПапкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem свойстваToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem переименоватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem большиеИконкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таблицаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem маленькиеИконкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem плиткаToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem поВозрастаниюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поУбываниюToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скрытыеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem системныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ImageList ilistTree;
        private System.Windows.Forms.ImageList ilistViewLarge;
        private System.Windows.Forms.ImageList ilistViewSmall;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelDetails;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ContextMenuStrip treeviewMenu;
        private System.Windows.Forms.ToolStripMenuItem добавитьВИзбранноеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьИзИзбранноеToolStripMenuItem;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.Button cmdClear;
        private System.Windows.Forms.ContextMenuStrip contextMenuFolder;
        private System.Windows.Forms.ToolStripMenuItem ContextвыделитьВсёToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextинвертироватьВыделениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem ContextвставитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem ContextнайтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem ContextтекущийВидToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextбольшиеИконкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextтаблицаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextмаленькиеИконкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextсписокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextплиткаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextоткрытьВНовомОкнеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextархивироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem ContextкопироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextвырезатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContextудалитьToolStripMenuItem;
    }
}

