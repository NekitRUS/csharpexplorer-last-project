﻿using System;
using System.Windows.Forms;
using System.IO;

namespace CSharp_Explorer
{
    static class Operations
    {
        public enum OperationOptions { Cut, Copy };
        public static event EventHandler FilesCountChanged = delegate { };

        private static System.Collections.Specialized.StringCollection filesToCutOrCopy = new System.Collections.Specialized.StringCollection();
        private static OperationOptions cutOrCopy;

        public static void Rename(string oldName, string newName)
        {
            string oldPath = oldName;
            string newPath = Path.Combine(oldName.Remove(oldName.Length - Path.GetFileName(oldName).Length), newName);
            bool isDirectory = (File.GetAttributes(oldPath) & FileAttributes.Directory) == FileAttributes.Directory;

            if (oldPath == newPath)
                throw new ArgumentNullException();

            if (string.IsNullOrEmpty(newName))
                throw new ArgumentException("Имя не может быть пустым.");
            foreach (char invalidChar in Path.GetInvalidFileNameChars())
                if (newName.Contains(invalidChar.ToString()))
                    throw new ArgumentException("Недопустимый символ в названии.");
            if (isDirectory && Directory.Exists(newPath))
                throw new ArgumentException("Папка с таким именем уже существует.");
            else if (!isDirectory && File.Exists(newPath))
                throw new ArgumentException("Файл с таким именем уже существует.");

            if (isDirectory)
                Directory.Move(oldPath, newPath);
            else
                File.Move(oldPath, newPath);
        }

        public static void CreateDirectory(string where, string name)
        {
            string newPath = Path.Combine(where, name);

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Имя папки не может быть пустым.");
            foreach (char invalidChar in Path.GetInvalidPathChars())
                if (name.Contains(invalidChar.ToString()))
                    throw new ArgumentException("Недопустимый символ в названии папки.");
            if (Directory.Exists(newPath))
                throw new ArgumentException("Папка с таким именем уже существует.");

            Directory.CreateDirectory(newPath);
        }

        public static void Delete(string path)
        {
            bool isDirectory = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;
            if (isDirectory)
                Directory.Delete(path, true);
            else
                File.Delete(path);
        }

        public static void ResetOperation()
        {
            filesToCutOrCopy.Clear();
            FilesCountChanged(filesToCutOrCopy, EventArgs.Empty);
        }

        public static void CutOrCopy(string path, OperationOptions option)
        {
            filesToCutOrCopy.Add(path);
            cutOrCopy = option;
            FilesCountChanged(filesToCutOrCopy, EventArgs.Empty);
        }

        public static void Paste(string destination)
        {
            if (filesToCutOrCopy.Count == 0)
                return;


            foreach (string path in filesToCutOrCopy)
            {
                try
                {
                    if (!File.Exists(path) && !Directory.Exists(path))
                        throw new ArgumentException(path + "\nЭлемент не найден.");

                    if ((Path.Combine(destination, new FileInfo(path).Name) == path) || (Path.Combine(destination, new DirectoryInfo(path).Name) == path))
                        continue;

                    bool isDirectory = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

                    if (!isDirectory && (File.Exists(Path.Combine(destination, new FileInfo(path).Name))))
                        if (MessageBox.Show(new FileInfo(path).Name + " \nФайл с таким именем уже существует.\nХотите его заменить?", "Замена файла", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            Delete(Path.Combine(destination, new FileInfo(path).Name));
                        else
                            continue;

                    if (isDirectory && (Directory.Exists(Path.Combine(destination, new DirectoryInfo(path).Name))))
                        if (MessageBox.Show(new DirectoryInfo(path).Name + " \nПапка с таким именем уже существует.\nХотите её заменить?", "Замена файла", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            Delete(Path.Combine(destination, new DirectoryInfo(path).Name));
                        else
                            continue;

                    if (!isDirectory)
                        File.Copy(path, Path.Combine(destination, new FileInfo(path).Name), false);
                    else if (isDirectory)
                    {
                        Directory.CreateDirectory(Path.Combine(destination, new DirectoryInfo(path).Name));
                        CopyDirectory(path, Path.Combine(destination, new DirectoryInfo(path).Name));
                    }

                    if (cutOrCopy == OperationOptions.Cut)
                        Delete(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }
            ResetOperation();
        }

        private static void CopyDirectory(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);

            foreach (string file in Directory.GetFiles(sourceDir))
            {
                string newName = Path.Combine(targetDir, new FileInfo(file).Name);
                try { File.Copy(file, newName, true); }
                catch (Exception) { continue; }
            }

            foreach (string directory in Directory.GetDirectories(sourceDir))
            {
                string newName = Path.Combine(targetDir, new DirectoryInfo(directory).Name);
                try
                {
                    if (Directory.Exists(newName))
                        throw new ArgumentException(newName + "\nПапка с таким именем уже существует.");
                }
                catch (Exception) { continue; }

                CopyDirectory(directory, newName);
            }
        }
    }
}