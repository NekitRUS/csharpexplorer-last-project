﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CSharp_Explorer
{
    public partial class FormReplace: Form
    {
        private RichTextBox textField;
        private bool firstSearch = true;

        public FormReplace(RichTextBox textField)
        {
            InitializeComponent();
            TopLevel = true;
            this.textField = textField;
        }

        private void txtWhat_TextChanged(object sender, EventArgs e)
        {
            cmdNext.Enabled = cmdReplace.Enabled = cmdReplaceAll.Enabled = txtWhat.Text != "";
        }

        private void cmdNext_Click(object sender, EventArgs e)
        {
            int startPosition = firstSearch ? textField.SelectionStart : textField.SelectionStart + 1;

            RichTextBoxFinds options = RichTextBoxFinds.None;
            if (checkRegister.Checked)
                options |= RichTextBoxFinds.MatchCase;

            int selectionStart = textField.Find(txtWhat.Text, startPosition, options);

            if (selectionStart == -1)
            {
                MessageBox.Show(this, String.Format("Не удается найти\n\"{0}\"", txtWhat.Text), "Замена", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            textField.Select(selectionStart, txtWhat.Text.Length);
            firstSearch = false;
        }

        private void cmdReplace_Click(object sender, EventArgs e)
        {
            bool withRegister = !checkRegister.Checked;

            if (String.Compare(textField.SelectedText, txtWhat.Text, withRegister) == 0)
                textField.SelectedText = txtWherewith.Text;

            cmdNext.PerformClick();
        }

        private void cmdReplaceAll_Click(object sender, EventArgs e)
        {
            int position = 0;
            int count = 0;
            int offset = txtWherewith.Text.Length - txtWhat.Text.Length;

            Regex regex;
            if (checkRegister.Checked)
                regex = new Regex(txtWhat.Text);
            else
                regex = new Regex(txtWhat.Text, RegexOptions.IgnoreCase);

            foreach (Match match in regex.Matches(textField.Text))
            {
                textField.Select(match.Index + position, txtWhat.Text.Length);
                position += offset;
                textField.SelectedText = txtWherewith.Text;
                count++;
            }

            MessageBox.Show(this, String.Format("Успешно заменено {0} совпадений!", count), "Замена", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormReplace_Deactivate(object sender, EventArgs e)
        {
            firstSearch = true;
        }
    }
}
