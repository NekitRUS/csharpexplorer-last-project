﻿namespace CSharp_Explorer
{
    partial class FormReplace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWhat = new System.Windows.Forms.Label();
            this.lblWherewith = new System.Windows.Forms.Label();
            this.txtWhat = new System.Windows.Forms.TextBox();
            this.txtWherewith = new System.Windows.Forms.TextBox();
            this.checkRegister = new System.Windows.Forms.CheckBox();
            this.cmdNext = new System.Windows.Forms.Button();
            this.cmdReplace = new System.Windows.Forms.Button();
            this.cmdReplaceAll = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblWhat
            // 
            this.lblWhat.AutoSize = true;
            this.lblWhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWhat.Location = new System.Drawing.Point(12, 9);
            this.lblWhat.Name = "lblWhat";
            this.lblWhat.Size = new System.Drawing.Size(35, 16);
            this.lblWhat.TabIndex = 0;
            this.lblWhat.Text = "Что:";
            this.lblWhat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWherewith
            // 
            this.lblWherewith.AutoSize = true;
            this.lblWherewith.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWherewith.Location = new System.Drawing.Point(12, 40);
            this.lblWherewith.Name = "lblWherewith";
            this.lblWherewith.Size = new System.Drawing.Size(37, 16);
            this.lblWherewith.TabIndex = 1;
            this.lblWherewith.Text = "Чем:";
            this.lblWherewith.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtWhat
            // 
            this.txtWhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtWhat.Location = new System.Drawing.Point(53, 4);
            this.txtWhat.Name = "txtWhat";
            this.txtWhat.Size = new System.Drawing.Size(301, 24);
            this.txtWhat.TabIndex = 2;
            this.txtWhat.TextChanged += new System.EventHandler(this.txtWhat_TextChanged);
            // 
            // txtWherewith
            // 
            this.txtWherewith.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtWherewith.Location = new System.Drawing.Point(53, 35);
            this.txtWherewith.Name = "txtWherewith";
            this.txtWherewith.Size = new System.Drawing.Size(301, 24);
            this.txtWherewith.TabIndex = 3;
            // 
            // checkRegister
            // 
            this.checkRegister.AutoSize = true;
            this.checkRegister.Location = new System.Drawing.Point(44, 119);
            this.checkRegister.Name = "checkRegister";
            this.checkRegister.Size = new System.Drawing.Size(120, 17);
            this.checkRegister.TabIndex = 4;
            this.checkRegister.Text = "С учетом регистра";
            this.checkRegister.UseVisualStyleBackColor = true;
            // 
            // cmdNext
            // 
            this.cmdNext.Enabled = false;
            this.cmdNext.Location = new System.Drawing.Point(371, 6);
            this.cmdNext.Name = "cmdNext";
            this.cmdNext.Size = new System.Drawing.Size(118, 28);
            this.cmdNext.TabIndex = 5;
            this.cmdNext.Text = "Найти далее";
            this.cmdNext.UseVisualStyleBackColor = true;
            this.cmdNext.Click += new System.EventHandler(this.cmdNext_Click);
            // 
            // cmdReplace
            // 
            this.cmdReplace.Enabled = false;
            this.cmdReplace.Location = new System.Drawing.Point(371, 40);
            this.cmdReplace.Name = "cmdReplace";
            this.cmdReplace.Size = new System.Drawing.Size(118, 28);
            this.cmdReplace.TabIndex = 6;
            this.cmdReplace.Text = "Заменить";
            this.cmdReplace.UseVisualStyleBackColor = true;
            this.cmdReplace.Click += new System.EventHandler(this.cmdReplace_Click);
            // 
            // cmdReplaceAll
            // 
            this.cmdReplaceAll.Enabled = false;
            this.cmdReplaceAll.Location = new System.Drawing.Point(371, 74);
            this.cmdReplaceAll.Name = "cmdReplaceAll";
            this.cmdReplaceAll.Size = new System.Drawing.Size(118, 28);
            this.cmdReplaceAll.TabIndex = 7;
            this.cmdReplaceAll.Text = "Заменить все";
            this.cmdReplaceAll.UseVisualStyleBackColor = true;
            this.cmdReplaceAll.Click += new System.EventHandler(this.cmdReplaceAll_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(371, 108);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(118, 28);
            this.cmdCancel.TabIndex = 8;
            this.cmdCancel.Text = "Отмена";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // FormReplace
            // 
            this.AcceptButton = this.cmdNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(512, 148);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdReplaceAll);
            this.Controls.Add(this.cmdReplace);
            this.Controls.Add(this.cmdNext);
            this.Controls.Add(this.checkRegister);
            this.Controls.Add(this.txtWherewith);
            this.Controls.Add(this.txtWhat);
            this.Controls.Add(this.lblWherewith);
            this.Controls.Add(this.lblWhat);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormReplace";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Заменить";
            this.Deactivate += new System.EventHandler(this.FormReplace_Deactivate);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWhat;
        private System.Windows.Forms.Label lblWherewith;
        private System.Windows.Forms.TextBox txtWhat;
        private System.Windows.Forms.TextBox txtWherewith;
        private System.Windows.Forms.CheckBox checkRegister;
        private System.Windows.Forms.Button cmdNext;
        private System.Windows.Forms.Button cmdReplace;
        private System.Windows.Forms.Button cmdReplaceAll;
        private System.Windows.Forms.Button cmdCancel;
    }
}