﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace CSharp_Explorer
{
    public partial class FormMain: Form
    {
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        #region Variables Declaration
        private string favPrefix = "fav*";
        private string searchPlaceholder = "Поиск в директории...";
        private int sortColumnNumber = -1;
        private string pathToFavotites = Path.Combine(Environment.CurrentDirectory, "Favorites.txt");
        public event EventHandler DirectoryChange = delegate { };
        TreeNode specialNode = null;
        private DirectoryInfo curDir = new DirectoryInfo(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)));
        private DirectoryInfo CurDir
        {
            get
            {
                return curDir;
            }
            set
            {
                if (value.Exists)
                {
                    curDir = value;
                    DirectoryChange(value, EventArgs.Empty);
                }
                else if (value.Root.Exists)
                {
                    CurDir = value.Root;
                    DirectoryChange(value, EventArgs.Empty);
                }
                else
                    CurDir = new DirectoryInfo(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)));
            }
        }
        #endregion

        public FormMain()
        {
            InitializeComponent();
            DirectoryChange += FormMain_DirectoryChange;
            Operations.FilesCountChanged += Operations_FilesCountChanged;
            CreateListViewHeaders();
            UpdateTree(treeView, ilistTree, true);
        }

        #region Methods
        void FormMain_DirectoryChange(object sender, EventArgs e)
        {
            try
            {
                DirectoryInfo newDirectory = sender as DirectoryInfo;

                treeView.BeginUpdate();

                if (treeView.SelectedNode != null)
                {
                    if (newDirectory.FullName != treeView.SelectedNode.Name)
                    {
                        string[] parts = newDirectory.FullName.Split('\\');
                        string part = parts[0] + "\\";
                        treeView.SelectedNode = treeView.Nodes.Find(part, true)[0];
                        treeView.SelectedNode.Expand();
                        for (int i = 1; i < parts.Length; i++)
                        {
                            part += parts[i];
                            treeView.Nodes.Find(part, true)[0].Expand();
                            part += "\\";
                        }
                        treeView.SelectedNode = treeView.Nodes.Find(newDirectory.FullName, true)[0];
                    }
                    else
                        treeView.SelectedNode.Collapse();
                }

                FillListView(newDirectory);
                txtPath.Text = newDirectory.FullName;
            }
            catch { CurDir = new DirectoryInfo(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System))); }
            finally { treeView.EndUpdate(); }
        }

        void Operations_FilesCountChanged(object sender, EventArgs e)
        {
            if ((sender as StringCollection).Count > 0)
                вставитьToolStripMenuItem.Enabled = ContextвставитьToolStripMenuItem.Enabled = true;
            else
                вставитьToolStripMenuItem.Enabled = ContextвставитьToolStripMenuItem.Enabled = false;
        }

        public void UpdateTree(TreeView tree, ImageList images, bool withFavorites)
        {
            images.Images.Clear();
            images.Images.Add("favourites_folder", global::CSharp_Explorer.Properties.Resources.favorites_folder);
            images.Images.Add("favourite", global::CSharp_Explorer.Properties.Resources.favourite);
            images.Images.Add("folder", global::CSharp_Explorer.Properties.Resources.folder);
            images.Images.Add("drive", global::CSharp_Explorer.Properties.Resources.drive);

            tree.BeginUpdate();
            tree.Nodes.Clear();
            if (withFavorites)
            {
                tree.Nodes.Add(new TreeNode { Text = "Избранное", Name = "Избранное", SelectedImageKey = "favourites_folder", ImageKey = "favourites_folder", ToolTipText = "Избранное" });
                FillFavorite();
            }

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                try
                {
                    if (!drive.IsReady)
                        continue;
                    tree.Nodes.Add(new TreeNode { Text = drive.Name, Name = drive.Name, SelectedImageKey = "drive", ImageKey = "drive", ToolTipText = drive.Name });
                }
                catch { continue; }
            }
            int start;
            if (withFavorites)
                start = 1;
            else
                start = 0;
            for (int i = start; i < tree.Nodes.Count; i++)
                try { FillNode(tree, tree.Nodes[i]); }
                catch { continue; }

            tree.SelectedNode = tree.Nodes.Find(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)), false)[0];
            tree.EndUpdate();
        }

        public void FillNode(TreeView tree, TreeNode node)
        {
            tree.BeginUpdate();
            node.Nodes.Clear();

            node.Nodes.AddRange(CreateNodes(node));
            foreach (TreeNode subNode in node.Nodes)
            {
                try { subNode.Nodes.AddRange(CreateNodes(subNode)); }
                catch { continue; }
            }

            tree.EndUpdate();
        }

        public TreeNode[] CreateNodes(TreeNode node)
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (DirectoryInfo dir in new DirectoryInfo(node.Name).GetDirectories())
            {
                nodes.Add(new TreeNode { Text = dir.Name, Name = dir.FullName, SelectedImageKey = "folder", ImageKey = "folder", ToolTipText = dir.FullName });
            }
            return nodes.ToArray();
        }

        private void FillFavorite()
        {
            if (!File.Exists(pathToFavotites))
            {
                File.Create(pathToFavotites);
                return;
            }

            treeView.BeginUpdate();
            treeView.Nodes["Избранное"].Nodes.Clear();
            try
            {
                StreamReader file = new StreamReader(pathToFavotites, System.Text.Encoding.GetEncoding(1251));
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] info = line.Split('*');
                    if (Directory.Exists(Path.Combine(info[1])))
                        treeView.Nodes["Избранное"].Nodes.Add(new TreeNode { Text = info[0], Name = favPrefix + Path.Combine(info[1]), SelectedImageKey = "favourite", ImageKey = "favourite", ToolTipText = "[Избранное]" + info[1] });
                    else
                        continue;
                }

                file.Close();
            }
            catch { }
            finally { treeView.EndUpdate(); }
        }

        private void CreateListViewHeaders()
        {
            lstvBrowser.Columns.Add(new ColumnHeader { Text = "Имя файла", Width = 260, TextAlign = HorizontalAlignment.Center });
            lstvBrowser.Columns.Add(new ColumnHeader { Text = "Тип", Width = 80, TextAlign = HorizontalAlignment.Center });
            lstvBrowser.Columns.Add(new ColumnHeader { Text = "Дата создания", Width = 120, TextAlign = HorizontalAlignment.Center });
            lstvBrowser.Columns.Add(new ColumnHeader { Text = "Размер, КБ", Width = 130, TextAlign = HorizontalAlignment.Center });
        }

        private void FillListView(DirectoryInfo directory)
        {
            ListViewItem item;
            ListViewItem.ListViewSubItem subItem;
            FileAttributes attributes;
            int foldersCount = 0;

            try
            {
                if (string.IsNullOrEmpty(directory.FullName))
                    return;

                lstvBrowser.BeginUpdate();
                lstvBrowser.Items.Clear();
                ilistViewLarge.Images.Clear();
                ilistViewSmall.Images.Clear();
                ilistViewLarge.Images.Add("folder", global::CSharp_Explorer.Properties.Resources.folder);
                ilistViewSmall.Images.Add("folder", global::CSharp_Explorer.Properties.Resources.folder);


                foreach (DirectoryInfo dir in directory.GetDirectories())
                {
                    attributes = dir.Attributes;
                    if ((((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.скрытыеToolStripMenuItem.Checked) && !((attributes & FileAttributes.System) == FileAttributes.System) ||
                       (((attributes & FileAttributes.System) == FileAttributes.System) && this.системныеToolStripMenuItem.Checked) && !((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) ||
                       (((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.скрытыеToolStripMenuItem.Checked && ((attributes & FileAttributes.System) == FileAttributes.System) && this.системныеToolStripMenuItem.Checked) ||
                       (!((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && !((attributes & FileAttributes.System) == FileAttributes.System)))
                    {
                        item = new ListViewItem { Text = dir.Name, Name = dir.FullName, ImageKey = "folder" };

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        lstvBrowser.Items.Add(item);
                    }
                }
                foldersCount = lstvBrowser.Items.Count;

                foreach (FileInfo file in directory.GetFiles())
                {
                    attributes = file.Attributes;
                    if ((((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.скрытыеToolStripMenuItem.Checked) && !((attributes & FileAttributes.System) == FileAttributes.System) ||
                       (((attributes & FileAttributes.System) == FileAttributes.System) && this.системныеToolStripMenuItem.Checked) && !((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) ||
                       (((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.скрытыеToolStripMenuItem.Checked && ((attributes & FileAttributes.System) == FileAttributes.System) && this.системныеToolStripMenuItem.Checked) ||
                       (!((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && !((attributes & FileAttributes.System) == FileAttributes.System)))
                    {
                        System.Drawing.Icon iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName);
                        ilistViewLarge.Images.Add(file.FullName, iconForFile);
                        ilistViewSmall.Images.Add(file.FullName, iconForFile);


                        item = new ListViewItem { Text = file.Name.Substring(0, file.Name.Length - file.Extension.Length), Name = file.FullName, ImageKey = file.FullName };

                        subItem = new ListViewItem.ListViewSubItem { Text = file.Extension };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = file.CreationTime.ToString() };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:#,#;#,#;0}", file.Length / 1024) };
                        item.SubItems.Add(subItem);

                        lstvBrowser.Items.Add(item);
                    }
                }

            }

            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            finally
            {
                lstvBrowser.EndUpdate();
                statusLabel.Text = "";
                if (foldersCount != 0)
                    statusLabel.Text += "Папок: " + foldersCount.ToString();
                int filesCount = lstvBrowser.Items.Count - foldersCount;
                if (filesCount > 0)
                    statusLabel.Text += " Файлов: " + filesCount.ToString();

                statusLabelDetails.Text = "";
                открытьВНовомОкнеToolStripMenuItem.Enabled = архивироватьToolStripMenuItem.Enabled =
                    свойстваToolStripMenuItem.Enabled = удалитьToolStripMenuItem.Enabled = переименоватьToolStripMenuItem.Enabled =
                    копироватьToolStripMenuItem.Enabled = вырезатьToolStripMenuItem.Enabled =
                    скопироватьВПапкуToolStripMenuItem.Enabled = переместитьВПапкуToolStripMenuItem.Enabled = false;
                for (int i = 0; i < lstvBrowser.Items.Count; i++)
                    lstvBrowser_ItemSelectionChanged(lstvBrowser, new ListViewItemSelectionChangedEventArgs(lstvBrowser.Items[i], i, false));
            }
        }

        private string AboutFolder(string folderFullName, string folderName)
        {
            string info = "";
            try
            {
                info += "\"" + folderName + "\" содержит " + Directory.GetDirectories(folderFullName).Length.ToString() + " папок и " + Directory.GetFiles(folderFullName).Length.ToString() + " файлов";
            }
            catch { }
            return info;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form textEditor = null;
            foreach (Form form in Application.OpenForms)
                if (form.Name == "CSharp_Text_Editor")
                    textEditor = form;

            if (textEditor == null)
                return;

            textEditor.Close();
            if (!textEditor.IsDisposed)
                e.Cancel = true;
        }
        #endregion

        #region Menu Events Handlers
        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about = string.Format("Файловый менеджер \"C# Explorer\" v.1.0.\n" +
                                        "Copyright © 2014 Титов Н.А. (гр. МА-12-1) [nekit94-08@mail.ru].\nДля использования исключительно в учебных целях.");
            MessageBox.Show(this, about, "About \"C# Explorer\"", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process openHelp = new Process();
                openHelp.StartInfo.FileName = "acrord32.exe";
                openHelp.StartInfo.Arguments = "\"" + Path.Combine(Directory.GetCurrentDirectory(), "help.pdf") + "\"";
                openHelp.Start();
                openHelp.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void строкаСостоянияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statusStrip.Visible = строкаСостоянияToolStripMenuItem.Checked)
                this.splitContainer.Height -= строкаСостоянияToolStripMenuItem.Height;
            else
                this.splitContainer.Height += строкаСостоянияToolStripMenuItem.Height;
        }

        private void таблицаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            большиеИконкиToolStripMenuItem.Checked = маленькиеИконкиToolStripMenuItem.Checked = списокToolStripMenuItem.Checked = плиткаToolStripMenuItem.Checked =
             ContextбольшиеИконкиToolStripMenuItem.Checked = ContextмаленькиеИконкиToolStripMenuItem.Checked = ContextсписокToolStripMenuItem.Checked = ContextплиткаToolStripMenuItem.Checked = false;
            таблицаToolStripMenuItem.Checked = ContextтаблицаToolStripMenuItem.Checked = true;
            lstvBrowser.View = View.Details;
        }

        private void большиеИконкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            таблицаToolStripMenuItem.Checked = маленькиеИконкиToolStripMenuItem.Checked = списокToolStripMenuItem.Checked = плиткаToolStripMenuItem.Checked =
              ContextтаблицаToolStripMenuItem.Checked = ContextмаленькиеИконкиToolStripMenuItem.Checked = ContextсписокToolStripMenuItem.Checked = ContextплиткаToolStripMenuItem.Checked = false;
            большиеИконкиToolStripMenuItem.Checked = ContextбольшиеИконкиToolStripMenuItem.Checked = true;
            lstvBrowser.View = View.LargeIcon;
        }

        private void маленькиеИконкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            таблицаToolStripMenuItem.Checked = большиеИконкиToolStripMenuItem.Checked = списокToolStripMenuItem.Checked = плиткаToolStripMenuItem.Checked =
                ContextтаблицаToolStripMenuItem.Checked = ContextбольшиеИконкиToolStripMenuItem.Checked = ContextсписокToolStripMenuItem.Checked = ContextплиткаToolStripMenuItem.Checked = false;
            маленькиеИконкиToolStripMenuItem.Checked = ContextмаленькиеИконкиToolStripMenuItem.Checked = true;
            lstvBrowser.View = View.SmallIcon;
        }

        private void списокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            таблицаToolStripMenuItem.Checked = большиеИконкиToolStripMenuItem.Checked = маленькиеИконкиToolStripMenuItem.Checked = плиткаToolStripMenuItem.Checked =
                ContextтаблицаToolStripMenuItem.Checked = ContextбольшиеИконкиToolStripMenuItem.Checked = ContextмаленькиеИконкиToolStripMenuItem.Checked = ContextплиткаToolStripMenuItem.Checked = false;
            списокToolStripMenuItem.Checked = ContextсписокToolStripMenuItem.Checked = true;
            lstvBrowser.View = View.List;
        }

        private void плиткаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            таблицаToolStripMenuItem.Checked = большиеИконкиToolStripMenuItem.Checked = маленькиеИконкиToolStripMenuItem.Checked = списокToolStripMenuItem.Checked =
                 ContextтаблицаToolStripMenuItem.Checked = ContextбольшиеИконкиToolStripMenuItem.Checked = ContextмаленькиеИконкиToolStripMenuItem.Checked = ContextсписокToolStripMenuItem.Checked = false;
            плиткаToolStripMenuItem.Checked = ContextплиткаToolStripMenuItem.Checked = true;
            lstvBrowser.View = View.Tile;
        }

        private void скрытыеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurDir = CurDir;
        }

        private void системныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurDir = CurDir;
        }

        private void поВозрастаниюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            поУбываниюToolStripMenuItem.Checked = false;
            поВозрастаниюToolStripMenuItem.Checked = true;
            lstvBrowser.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Ascending);
        }

        private void поУбываниюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            поВозрастаниюToolStripMenuItem.Checked = false;
            поУбываниюToolStripMenuItem.Checked = true;
            lstvBrowser.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);
        }

        private void обновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oldPath = treeView.SelectedNode.Name;
            UpdateTree(treeView, ilistTree, true);
            if (Directory.Exists(oldPath))
                CurDir = new DirectoryInfo(oldPath);
        }

        private void выделитьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstvBrowser.Focus();
            foreach (ListViewItem item in lstvBrowser.Items)
                item.Selected = true;
        }

        private void инвертироватьВыделениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstvBrowser.Focus();
            foreach (ListViewItem item in lstvBrowser.Items)
                item.Selected = !item.Selected;
        }

        private void открытьВНовомОкнеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstvBrowser_ItemActivate(lstvBrowser, EventArgs.Empty);
        }

        private void архивироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filesToArchive = "";
            string zipName = "";

            if (lstvBrowser.SelectedItems.Count == 1)
                zipName += CurDir.FullName + "\\" + lstvBrowser.SelectedItems[0].Text + ".zip";
            else
                zipName += CurDir.FullName + "\\" + CurDir.Name + ".zip";

            if (File.Exists(zipName))
            {
                DialogResult answer = MessageBox.Show(this, "Файл с таким именем уже существует.\nХотите его заменить?", "Замена файла", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (answer == DialogResult.No)
                    return;
                else
                    try { File.Delete(zipName); }
                    catch { }
            }

            foreach (ListViewItem item in lstvBrowser.Items)
                if (item.Selected)
                    filesToArchive += "\"" + item.Name + "\" ";

            Process WinRAR = new Process();
            WinRAR.StartInfo.FileName = "WinRAR.exe";
            WinRAR.StartInfo.CreateNoWindow = true;
            WinRAR.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            WinRAR.StartInfo.Arguments = "a -ep1 -r -IBCK " + "\"" + zipName + "\" " + filesToArchive;

            Process WinZip = new Process();
            WinZip.StartInfo.FileName = "winzip32.exe";
            WinZip.StartInfo.CreateNoWindow = true;
            WinZip.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            WinZip.StartInfo.Arguments = "-min -a -r " + "\"" + zipName + "\" " + filesToArchive;

            Process SevenZ = new Process();
            SevenZ.StartInfo.FileName = "C:\\Program Files\\7-Zip\\7z.exe";
            SevenZ.StartInfo.CreateNoWindow = true;
            SevenZ.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            SevenZ.StartInfo.Arguments = "a " + "\"" + zipName + "\" " + filesToArchive;

            try
            {
                WinRAR.Start();
                WinRAR.WaitForExit();
            }
            catch (Win32Exception)
            {
                try
                {
                    WinZip.Start();
                    WinZip.WaitForExit();
                }
                catch (Win32Exception)
                {
                    try
                    {
                        SevenZ.Start();
                        SevenZ.WaitForExit();
                    }
                    catch (Win32Exception) { MessageBox.Show("Не найдена программа для архивации!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally
            {
                WinRAR.Close();
                WinZip.Close();
                SevenZ.Close();
                CurDir = CurDir;
            }
        }

        private void свойстваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FileSystemInfo selectedItem;

                if (lstvBrowser.SelectedItems[0].ImageKey == "folder")
                    selectedItem = new DirectoryInfo(lstvBrowser.SelectedItems[0].Name);
                else
                    selectedItem = new FileInfo(lstvBrowser.SelectedItems[0].Name);

                new FormProperties(selectedItem).ShowDialog();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void новаяПапкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { new FormRenameOrCreate(FormRenameOrCreate.FormOptions.Create, CurDir.FullName).ShowDialog(); }
            catch (ArgumentNullException) { return; }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            обновитьToolStripMenuItem.PerformClick();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Вы действительно хотите удалить выбранные элементы?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            bool wasDirectory = false;

            foreach (ListViewItem item in lstvBrowser.SelectedItems)
            {
                try
                {
                    Operations.Delete(item.Name);
                    wasDirectory = item.ImageKey == "folder" ? true : wasDirectory;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }

            if (wasDirectory)
                обновитьToolStripMenuItem.PerformClick();
            else
                CurDir = CurDir;
        }

        private void переименоватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool wasDirectory = lstvBrowser.SelectedItems[0].ImageKey == "folder";

            try { new FormRenameOrCreate(FormRenameOrCreate.FormOptions.Rename, lstvBrowser.SelectedItems[0].Name).ShowDialog(); }
            catch (ArgumentNullException) { return; }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            if (wasDirectory)
                обновитьToolStripMenuItem.PerformClick();
            else
                CurDir = CurDir;
        }

        private void найтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtSearch.Focus();
            txtSearch.SelectAll();
        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operations.ResetOperation();
            foreach (ListViewItem item in lstvBrowser.SelectedItems)
            {
                try { Operations.CutOrCopy(item.Name, Operations.OperationOptions.Copy); }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operations.ResetOperation();
            foreach (ListViewItem item in lstvBrowser.SelectedItems)
            {
                try { Operations.CutOrCopy(item.Name, Operations.OperationOptions.Cut); }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }
        }

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operations.Paste(CurDir.FullName);
            CurDir = CurDir;
        }

        private void переместитьВПапкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operations.ResetOperation();
            foreach (ListViewItem item in lstvBrowser.SelectedItems)
            {
                try { Operations.CutOrCopy(item.Name, Operations.OperationOptions.Cut); }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }

            FormFolderOperations toolForm = new FormFolderOperations("Переместить в...");
            toolForm.ShowDialog();
            if (toolForm.DialogResult == DialogResult.OK)
            {
                Operations.Paste(toolForm.FolderPath);
                CurDir = CurDir;
            }
            Operations.ResetOperation();
        }

        private void скопироватьВПапкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operations.ResetOperation();
            foreach (ListViewItem item in lstvBrowser.SelectedItems)
            {
                try { Operations.CutOrCopy(item.Name, Operations.OperationOptions.Copy); }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }

            FormFolderOperations toolForm = new FormFolderOperations("Скопировать в...");
            toolForm.ShowDialog();
            if (toolForm.DialogResult == DialogResult.OK)
            {
                Operations.Paste(toolForm.FolderPath);
            }
            Operations.ResetOperation();
        }

        private void текстовыйРедакторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CSharp_Text_Editor.GetEditor.AddTab();
        }
        #endregion

        #region List View Events Handlers
        private void lstvBrowser_ItemActivate(object sender, EventArgs e)
        {
            string folder = "";
            foreach (ListViewItem item in ((ListView)sender).SelectedItems)
            {
                try
                {
                    if (item.ImageKey != "folder")
                    {
                        if (Path.GetExtension(item.Name).ToLower() == ".txt")
                        {
                            CSharp_Text_Editor.GetEditor.AddTab(item.Name);
                        }
                        else
                            Process.Start(item.Name);
                    }
                    else
                        folder = item.Name;
                }
                catch (Win32Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }

            if (folder != "")
            {
                folder = Path.Combine(folder);
                CurDir = new DirectoryInfo(folder);
            }
        }

        private void lstvBrowser_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListView.SelectedListViewItemCollection selectedItems = lstvBrowser.SelectedItems;
            if (selectedItems.Count != 0)
            {
                открытьВНовомОкнеToolStripMenuItem.Enabled =
                    архивироватьToolStripMenuItem.Enabled =
                    удалитьToolStripMenuItem.Enabled =
                    переименоватьToolStripMenuItem.Enabled =
                    копироватьToolStripMenuItem.Enabled =
                    вырезатьToolStripMenuItem.Enabled =
                    скопироватьВПапкуToolStripMenuItem.Enabled =
                    переместитьВПапкуToolStripMenuItem.Enabled = true;
            }
            else
            {
                открытьВНовомОкнеToolStripMenuItem.Enabled =
                    архивироватьToolStripMenuItem.Enabled =
                    удалитьToolStripMenuItem.Enabled =
                    переименоватьToolStripMenuItem.Enabled =
                    копироватьToolStripMenuItem.Enabled =
                    вырезатьToolStripMenuItem.Enabled =
                    скопироватьВПапкуToolStripMenuItem.Enabled =
                    переместитьВПапкуToolStripMenuItem.Enabled = false;
            }

            statusLabelDetails.Text = "";
            string toAdd = " Выбрано элементов: ";
            if (statusLabel.Text.Contains(toAdd))
                statusLabel.Text = statusLabel.Text.Remove(statusLabel.Text.LastIndexOf(toAdd));
            if (selectedItems.Count > 1)
                statusLabel.Text += toAdd + selectedItems.Count.ToString();

            if (selectedItems.Count == 1)
            {
                свойстваToolStripMenuItem.Enabled = переименоватьToolStripMenuItem.Enabled = true;

                if (selectedItems[0].ImageKey == "folder")
                    statusLabelDetails.Text += AboutFolder(selectedItems[0].Name, selectedItems[0].Text);
                else
                    statusLabelDetails.Text += "Файл \"" + selectedItems[0].Text + selectedItems[0].SubItems[1].Text + "\" имеет размер: " + selectedItems[0].SubItems[3].Text + "Кб и был создан " + selectedItems[0].SubItems[2].Text;
            }
            else
                свойстваToolStripMenuItem.Enabled = переименоватьToolStripMenuItem.Enabled = false;
        }

        private void lstvBrowser_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            поВозрастаниюToolStripMenuItem.Checked = поУбываниюToolStripMenuItem.Checked = false;

            if (e.Column != sortColumnNumber)
            {
                sortColumnNumber = e.Column;
                lstvBrowser.Sorting = SortOrder.Ascending;
            }
            else
            {
                if (lstvBrowser.Sorting == SortOrder.Ascending)
                    lstvBrowser.Sorting = SortOrder.Descending;
                else
                    lstvBrowser.Sorting = SortOrder.Ascending;
            }

            if ((e.Column == 0) && (lstvBrowser.Sorting == SortOrder.Ascending))
                поВозрастаниюToolStripMenuItem.Checked = true;
            if ((e.Column == 0) && (lstvBrowser.Sorting == SortOrder.Descending))
                поУбываниюToolStripMenuItem.Checked = true;

            lstvBrowser.ListViewItemSorter = new ListViewItemComparer(e.Column, lstvBrowser.Sorting);

            if (lstvBrowser.SelectedItems.Count != 0)
                lstvBrowser.SelectedItems[0].EnsureVisible();
        }

        private void lstvBrowser_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                System.Threading.Thread.Sleep(100);
                lstvBrowser.DoDragDrop(lstvBrowser.SelectedItems, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private void lstvBrowser_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g = lstvBrowser.CreateGraphics();
            foreach (ListViewItem item in lstvBrowser.Items)
                g.DrawRectangle(new Pen(Color.White), item.Bounds);
        }

        private void lstvBrowser_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                const Single scrollRegion = 20;
                Point pt = lstvBrowser.PointToClient(Cursor.Position);
                if ((pt.Y + scrollRegion) > lstvBrowser.Height)
                    SendMessage(lstvBrowser.Handle, 277, 1, IntPtr.Zero);
                else if (pt.Y < (lstvBrowser.Top + scrollRegion))
                    SendMessage(lstvBrowser.Handle, 277, 0, IntPtr.Zero);

                if (!e.Data.GetDataPresent(typeof(ListView.SelectedListViewItemCollection)))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }

                ListViewItem itemUnder = lstvBrowser.GetItemAt(lstvBrowser.PointToClient(new Point(e.X, e.Y)).X, lstvBrowser.PointToClient(new Point(e.X, e.Y)).Y);
                if ((itemUnder == null) || ((itemUnder.ImageKey != "folder") && (Path.GetExtension(itemUnder.Name).ToLower() != ".exe")))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }

                if (((e.KeyState & 4) == 4) && ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move))// SHIFT
                    e.Effect = DragDropEffects.Move;
                else if (((e.KeyState & 8) == 8) && ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy))// CTL
                    e.Effect = DragDropEffects.Copy;
                else if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)//DEFAULT
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;


                Graphics g = lstvBrowser.CreateGraphics();
                foreach (ListViewItem item in lstvBrowser.Items)
                    g.DrawRectangle(new Pen(Color.White), item.Bounds);
                g.DrawRectangle(new Pen(Color.Black), itemUnder.Bounds);
            }
            catch { }
        }

        private void lstvBrowser_DragDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(ListView.SelectedListViewItemCollection)))
                return;

            ListView.SelectedListViewItemCollection items = (ListView.SelectedListViewItemCollection)e.Data.GetData(typeof(ListView.SelectedListViewItemCollection));
            ListViewItem targetItem = lstvBrowser.GetItemAt(lstvBrowser.PointToClient(new Point(e.X, e.Y)).X, lstvBrowser.PointToClient(new Point(e.X, e.Y)).Y);
            if (targetItem == null)
                return;

            if (targetItem.ImageKey == "folder")
            {
                if (e.Effect == DragDropEffects.Copy)
                {
                    foreach (ListViewItem path in items)
                        if (path.Name != targetItem.Name)
                            try { Operations.CutOrCopy(path.Name, Operations.OperationOptions.Copy); }
                            catch { continue; }
                    Operations.Paste(targetItem.Name);
                }

                if (e.Effect == DragDropEffects.Move)
                {
                    foreach (ListViewItem path in items)
                        if (path.Name != targetItem.Name)
                            try { Operations.CutOrCopy(path.Name, Operations.OperationOptions.Cut); }
                            catch { continue; }
                    Operations.Paste(targetItem.Name);
                    CurDir = CurDir;
                }
            }
            else if (Path.GetExtension(targetItem.Name).ToLower() == ".exe")
            {
                try
                {
                    Process process = new Process();
                    string filesToOpen = "";
                    foreach (ListViewItem path in items)
                        filesToOpen += "\"" + path.Name + "\" ";
                    process.StartInfo = new ProcessStartInfo(fileName : targetItem.Name, arguments : filesToOpen);
                    process.Start();
                }
                catch { }
            }
        }
        #endregion

        #region Tree View Events Handlers
        private void treeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if ((e.Node.ImageKey != "favourites_folder") && Directory.Exists(e.Node.Name))
                FillNode(treeView, e.Node);
        }

        private void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.Name == "Избранное")
            {
                if (e.Node.IsExpanded)
                {
                    e.Node.Collapse();
                    treeView.SelectedNode = e.Node.NextNode;
                }
                else
                    e.Node.Expand();

                e.Cancel = true;
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            foreach (ListViewItem item in lstvBrowser.Items)
                item.Selected = false;

            CurDir = new DirectoryInfo(e.Node.Name.Replace(favPrefix, ""));
            e.Node.EnsureVisible();
        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                specialNode = treeView.GetNodeAt(e.Location);
                if (specialNode != null)
                    if (specialNode.ImageKey == "favourites_folder")
                        specialNode = null;

                treeviewMenu.Show(treeView, e.Location);
            }
        }

        private void treeView_MouseLeave(object sender, EventArgs e)
        {
            mouse_event(0x02 | 0x04, Cursor.Position.X, Cursor.Position.Y, 0, 0);
        }
        #endregion

        #region Tree View Context Menu Events Handlers
        private void treeviewMenu_Opening(object sender, CancelEventArgs e)
        {
            if (specialNode == null)
            {
                e.Cancel = true;
                return;
            }

            if (specialNode.ImageKey == "favourite")
            {
                добавитьВИзбранноеToolStripMenuItem.Visible = false;
                удалитьИзИзбранноеToolStripMenuItem.Visible = true;
            }
            else
            {
                добавитьВИзбранноеToolStripMenuItem.Visible = true;
                удалитьИзИзбранноеToolStripMenuItem.Visible = false;
            }
        }

        private void добавитьВИзбранноеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!File.Exists(pathToFavotites))
                    File.Create(pathToFavotites);

                bool alreadyExists = false;
                using (StreamReader file = new StreamReader(pathToFavotites, System.Text.Encoding.GetEncoding(1251)))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                        if (line.Split('*')[1] == specialNode.Name)
                        {
                            alreadyExists = true;
                            break;
                        }
                }

                if (alreadyExists)
                    return;

                using (StreamWriter append = new StreamWriter(pathToFavotites, true, System.Text.Encoding.GetEncoding(1251)))
                    append.WriteLine(specialNode.Text + "*" + specialNode.Name);

                specialNode = null;

                FillFavorite();
            }
            catch { }
        }

        private void удалитьИзИзбранноеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!File.Exists(pathToFavotites))
                    return;

                List<string> favorites = new List<string>();
                favorites.AddRange(File.ReadAllLines(pathToFavotites, System.Text.Encoding.GetEncoding(1251)));
                File.Delete(pathToFavotites);

                using (StreamWriter replace = new StreamWriter(pathToFavotites, false, System.Text.Encoding.GetEncoding(1251)))
                {
                    foreach (string line in favorites)
                    {
                        if (line.Split('*')[1] == specialNode.Name.Replace(favPrefix, ""))
                            continue;

                        replace.WriteLine(line);
                    }
                }

                specialNode = null;

                FillFavorite();
            }
            catch { }
        }
        #endregion

        #region Navigation Bar Events Handlers
        private void txtPath_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C))
                txtPath.Copy();
            if (e.Control && (e.KeyCode == Keys.V))
                txtPath.Paste();
            if (e.Control && (e.KeyCode == Keys.X))
                txtPath.Cut();
        }

        private void txtPath_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    txtPath.Text = ((txtPath.Text[txtPath.Text.Length - 1] == '\\') && (txtPath.Text[txtPath.Text.Length - 2] != '\\')) ? txtPath.Text.Remove(txtPath.Text.Length - 1) : txtPath.Text;
                    DirectoryInfo newDir = new DirectoryInfo(Path.Combine(txtPath.Text));
                    if (newDir.Exists)
                    {
                        CurDir = newDir;
                        lstvBrowser.Focus();
                    }
                    else
                    {
                        MessageBox.Show(this, "Не найдена папка\n" + txtPath.Text, "Папка не найдена", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        lstvBrowser.Focus();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "Не найдена папка\n" + txtPath.Text, "Папка не найдена", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    lstvBrowser.Focus();
                }
            }
        }

        private void txtPath_Leave(object sender, EventArgs e)
        {
            txtPath.Text = CurDir.FullName;
        }

        private void txtSearch_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C))
                txtSearch.Copy();
            if (e.Control && (e.KeyCode == Keys.V))
                txtSearch.Paste();
            if (e.Control && (e.KeyCode == Keys.X))
                txtSearch.Cut();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmdSearch.PerformClick();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            if (txtSearch.Text == searchPlaceholder)
                txtSearch.Clear();

            cmdClear.Visible = cmdClear.Enabled = true;
            txtSearch.ForeColor = SystemColors.WindowText;
            txtSearch.Font = new Font(txtSearch.Font, FontStyle.Regular);
        }

        private void txtSearch_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSearch.Text))
            {
                txtSearch.Text = searchPlaceholder;
                txtSearch.ForeColor = Color.Gray;
                txtSearch.Font = new Font(txtSearch.Font, FontStyle.Italic);
            }
        }

        private void cmdClear_Click(object sender, EventArgs e)
        {
            txtSearch.Clear();
            cmdClear.Visible = cmdClear.Enabled = false;
            CurDir = CurDir;
            txtSearch_Leave(sender, e);
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string pattern = txtSearch.Text.Trim();

                if (string.IsNullOrWhiteSpace(pattern) || (pattern == searchPlaceholder))
                {
                    cmdClear.Visible = false;
                    return;
                }

                List<ListViewItem> results = new List<ListViewItem>();
                Regex search = new Regex(pattern, RegexOptions.IgnoreCase);
                for (int index = 0; index < lstvBrowser.Items.Count; index++)
                {
                    Match result = search.Match(lstvBrowser.Items[index].Text + lstvBrowser.Items[index].SubItems[1].Text);
                    if (result.Success)
                        results.Add(lstvBrowser.Items[index]);
                }

                lstvBrowser.BeginUpdate();
                lstvBrowser.Items.Clear();
                lstvBrowser.Items.AddRange(results.ToArray());
                lstvBrowser.EndUpdate();
            }
            catch { return; }
        }
        #endregion

        #region List View Context Menu Events Handlers
        private void contextMenuFolder_Opening(object sender, CancelEventArgs e)
        {
            bool anyItemSelected = lstvBrowser.SelectedItems.Count != 0;

            ContextоткрытьВНовомОкнеToolStripMenuItem.Visible =
                ContextархивироватьToolStripMenuItem.Visible =
                ContextкопироватьToolStripMenuItem.Visible =
                ContextвырезатьToolStripMenuItem.Visible =
                ContextудалитьToolStripMenuItem.Visible =
                toolStripSeparator16.Visible = anyItemSelected;

            ContextвыделитьВсёToolStripMenuItem.Visible =
                ContextинвертироватьВыделениеToolStripMenuItem.Visible =
                ContextвставитьToolStripMenuItem.Visible =
                ContextнайтиToolStripMenuItem.Visible =
                ContextтекущийВидToolStripMenuItem.Visible =
                toolStripSeparator13.Visible =
                toolStripSeparator14.Visible =
                toolStripSeparator15.Visible = !anyItemSelected;
        }

        private void ContextвыделитьВсёToolStripMenuItem_Click(object sender, EventArgs e)
        {
            выделитьВсеToolStripMenuItem.PerformClick();
        }

        private void ContextинвертироватьВыделениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            инвертироватьВыделениеToolStripMenuItem.PerformClick();
        }

        private void ContextвставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            вставитьToolStripMenuItem.PerformClick();
        }

        private void ContextнайтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            найтиToolStripMenuItem.PerformClick();
        }

        private void ContextоткрытьВНовомОкнеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            открытьВНовомОкнеToolStripMenuItem.PerformClick();
        }

        private void ContextархивироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            архивироватьToolStripMenuItem.PerformClick();
        }

        private void ContextкопироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            копироватьToolStripMenuItem.PerformClick();
        }

        private void ContextвырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            вырезатьToolStripMenuItem.PerformClick();
        }

        private void ContextудалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            удалитьToolStripMenuItem.PerformClick();
        }

        private void ContextбольшиеИконкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            большиеИконкиToolStripMenuItem.PerformClick();
        }

        private void ContextтаблицаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            таблицаToolStripMenuItem.PerformClick();
        }

        private void ContextмаленькиеИконкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            маленькиеИконкиToolStripMenuItem.PerformClick();
        }

        private void ContextсписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            списокToolStripMenuItem.PerformClick();
        }

        private void ContextплиткаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            плиткаToolStripMenuItem.PerformClick();
        }
        #endregion
    }
}