﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CSharp_Explorer
{
    public partial class FormProperties: Form
    {
        public FormProperties(FileSystemInfo itemInformation)
        {
            InitializeComponent();
            this.Text = "Свойства: " + itemInformation.Name;

            ListViewItem newItem;
            ListViewItem.ListViewSubItem newSubItem;

            newItem = new ListViewItem { Text = "Имя" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.Name };
            newItem.SubItems.Add(newSubItem);
            lstvProperties .Items.Add(newItem);

            newItem = new ListViewItem { Text = "Тип" };
            if (itemInformation is FileInfo)
                newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.Extension };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Папка" };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Полный путь" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.FullName };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Размер" };
            long fullSize;
            if (itemInformation is FileInfo)
                fullSize = ((FileInfo)itemInformation).Length;
            else
                fullSize = DirectorySize(itemInformation as DirectoryInfo);
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:#,#;#,#;0}", fullSize) + " байт" };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Дата создания" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:G}", itemInformation.CreationTime) };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Дата изменения" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:G}", itemInformation.LastWriteTime) };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Скрытый" };
            if ((itemInformation.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Да" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Нет" };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Только чтение" };
            if ((itemInformation.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Да" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Нет" };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Системный" };
            if ((itemInformation.Attributes & FileAttributes.System) == FileAttributes.System)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Да" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Нет" };
            newItem.SubItems.Add(newSubItem);
            lstvProperties.Items.Add(newItem);
        }

        private long DirectorySize(DirectoryInfo name)
        {
            long totalSize = 0;
            foreach (FileInfo file in name.GetFiles())
            {
                try { totalSize += file.Length; }
                catch { continue; }
            }
            foreach (DirectoryInfo dir in name.GetDirectories())
            {
                try { totalSize += DirectorySize(dir); }
                catch { continue; }
            }
            return totalSize;
        }
    }
}
