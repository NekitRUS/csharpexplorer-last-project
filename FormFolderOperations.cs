﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CSharp_Explorer
{
    public partial class FormFolderOperations: Form
    {
        public string FolderPath { get; private set; }

        public FormFolderOperations(string caption)
        {
            InitializeComponent();
            this.Text = caption;
            try { Program.mainWindow.UpdateTree(treeViewFolders, ilistFolders, false); }
            catch { }
        }

        private void treeViewFolders_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (Directory.Exists(e.Node.Name))
                try { Program.mainWindow.FillNode(treeViewFolders, e.Node); }
                catch { }
        }

        private void treeViewFolders_AfterSelect(object sender, TreeViewEventArgs e)
        {
            txtPath.Text = e.Node.Name;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            FolderPath = Path.Combine(treeViewFolders.SelectedNode.Name);
        }
    }
}
