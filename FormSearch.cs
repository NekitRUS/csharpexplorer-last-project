﻿using System;
using System.Windows.Forms;

namespace CSharp_Explorer
{
    public partial class FormSearch: Form
    {
        private RichTextBox textField;
        private bool firstPaint = true;
        private bool firstSearch = true;

        public FormSearch()
        {
            InitializeComponent();
            TopLevel = true;
        }

        void CSharpTextEditor_OnFindClick(object sender, CSharp_Text_Editor.FindClickEventArgs e)
        {
            textField = sender as RichTextBox;
            if (e.ClickNeeded)
                cmdSearch_Click(cmdSearch, EventArgs.Empty);
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
                return;

            int startPosition = firstSearch ? textField.SelectionStart : textField.SelectionStart + 1;
            int endPosition = textField.TextLength;

            RichTextBoxFinds options = RichTextBoxFinds.None;
            if (checkRegister.Checked)
                options |= RichTextBoxFinds.MatchCase;
            if (radioUp.Checked)
            {
                startPosition = 0;
                endPosition = textField.SelectionStart;
                options |= RichTextBoxFinds.Reverse;
            }

            int selectionStart = textField.Find(txtSearch.Text, startPosition, endPosition, options);

            if (selectionStart == -1)
            {
                MessageBox.Show(this, String.Format("Не удается найти\n\"{0}\"", txtSearch.Text), "Поиск", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            textField.Select(selectionStart, txtSearch.Text.Length);
            firstSearch = false;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            firstSearch = true;
            this.Hide();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            cmdSearch.Enabled = txtSearch.Text != "";
        }

        private void frmSearch_Paint(object sender, PaintEventArgs e)
        {
            if (firstPaint)
                CSharp_Text_Editor.GetEditor.OnFindClick += CSharpTextEditor_OnFindClick;
            firstPaint = false;
        }

        private void frmSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            firstSearch = true;
            this.Hide();
            if (e.CloseReason == CloseReason.UserClosing)
                e.Cancel = true;
        }

        private void frmSearch_Deactivate(object sender, EventArgs e)
        {
            firstSearch = true;
        }
    }
}
